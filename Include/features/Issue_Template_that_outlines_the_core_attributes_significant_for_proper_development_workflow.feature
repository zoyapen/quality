Feature: Issue Template that outlines the core attributes significant for proper development workflow
  

  Scenario Outline: Issue Template that outlines the core attribute
    Given a story is needing to be created for developement
     And given a meaningful "Summary" , "User Story"
    When the story is actively being worked
    Then the story team develops the "Acceptance Criteria" that describe the conditions that have to be fulfilled so that the story is done
    When team starts work on the story, they will start filling in other <attributes>
     And QA will use that information to start creating the "Scenarios" using qTest Scerario
     And QA will start working on the testing framework and test management using qTest Manager
    When the story is ready to be tested
    Then QA will run the "Test Execution" viewable under "qTest Test Execution" within the Story
    When all test events have "Pass"
    Then the story is moved "DONE"
    
    Examples:
    | attributes |
    | Development Notes |
    | Precondition |
    | How to Test (if new functionality) |