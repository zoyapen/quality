Feature: Data created in Salesforce after AMF processing 
As a member services staff, after a member has paid their AMF, I should see all subscription and other subscription related data gets created correctly so that the member can be processed correctly.

  Scenario: Customer pay AMF for becoming Associate of ISC2
    Given person has passed exam with associate intent
    When person pay outstanding AMF through shopping cart
    Then subscription record is created
    And active Associate Membership badge is created
    And receipt, Receipt line, Transaction, Transaction Line, subscription and subscription Term record is created
  
  Scenario: Customer pay AMF for becoming Member of ISC2
    Given person has passed exam with full membership intent
    And ER has been approved
    When person pay outstanding AMF through shopping cart
    Then subscription record is created
    And active Professional Membership badge is created
    And receipt, Receipt line, Transaction, Transaction Line, subscription and subscription Term record is created
      
  Scenario: Member paid future AMF in middle of current active subscription
    Given member has active subscription
    And doesn't have any oustanding AMF
    When member pay next term(12 months) AMF through shopping cart
    Then subscription end date gets extended till end date of next term 
    And Professional Membership badge is still active
    And receipt, Receipt line, Transaction, Transaction Line, subscription and subscription Term record is created
    
  Scenario: Member paid outstanding AMF when in subscription grace period
    Given member has active subscription
    And current date has passed subscription end date
    And date lapsed field value is less than -91
    And has oustanding AMF
    When member pay next term(12 months) AMF through shopping cart
    Then subscription end date gets extended till end date of next term
    And new subscription term has consecutive start date 
    And Professional Membership badge is still active
    And receipt, Receipt line, Transaction, Transaction Line, subscription and subscription Term record is created
