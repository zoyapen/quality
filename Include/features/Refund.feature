Feature: Refund
  <Some interesting description here>

  Scenario: Create a refund for the member on the recently posted receipt
    Given member has reciept with the posted status
    When new Refund on the receipt is created
    And Refund Processed
    Then The refund receipt has a related refund epayment for the correct amount and with status of “Succeeded” has been created
    And The refund receipt has related transaction lines.

