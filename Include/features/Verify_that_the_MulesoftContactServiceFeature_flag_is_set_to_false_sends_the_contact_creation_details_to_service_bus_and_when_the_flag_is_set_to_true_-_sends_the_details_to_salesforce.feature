Feature: Verify that the MulesoftContactServiceFeature flag is set to false sends the contact creation details to service bus and when the flag is set to true - sends the details to salesforce

  Scenario: Verify that creating account in sitecore sends the details to service bus when the MulesoftContactServiceFeature flag is set to false
    Given I'm on Sitecore
    And I create a new account 
    And I navigate to the service bus
    Then I see the contact details in contact log for the above account

  Scenario: Verify that creating account in sitecore sends the details to salesForce when the MulesoftContactServiceFeature flag is set to true
    Given I'm on Sitecore
    And I create a new account 
    And I log into salesForce
    And I search for the above contact
    Then I see the contact is returned in the search result
    And I open the contact
    Then I see the correct contact details 

