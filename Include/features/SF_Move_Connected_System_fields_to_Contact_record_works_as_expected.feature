Feature: SF: Move Connected System fields to Contact record works as expected
  
  Scenario: Verify that Connected System Status record fields on Contact record in User Information Section - 5 statuses and Last Async event date is displayed in SalesForce for non-existing email
  	Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set a non-existing email address in the email field
    Then I receive a response with StatusCode: 201
    And a ReturnMessage: "Success! New Records Created."
    And ContactId = Legacy_ID__c on created Contact
    And SFId = Id of created Contact
    And I see Connected System Status no longer a separate object
    And I navigate to Salesforce QA
    And I search for the above contact
    Then I see the above contact in SalesForce
    And I open the above contact
    And I scroll down to the user information section
    Then I see the connected system status record
    And I do not see the connected system status record in the related list
    
Scenario: Verify that Email on existing Contact (1 or more) = Contact not created:
  	Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set a existing email address in the email field
    Then I see StatusCode: 400
    And ReturnMessage: "Contact already exists with this Email.”
    
Scenario: Verify that Email not on existing Contact, on 1 existing lead = contact create
  	Given I'm on SiteCore QA
  	And I navigate to a lead form
  	And I set the lead form email address to "cmr315LF@mailinator.com"
  	And I submit a lead form
  	Then I see "Thank you" message after submission
  	And I launch postman
  	And I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the email field to "cmr315LF@mailinator.com"
    Then I receive a response with StatusCode: 201
    And I see a ReturnMessage: "Success! Existing Lead Converted and New Records Created.”
    And the ContactId = Legacy_ID__c on created Contact
    And the SFId = Id of created Contact which must also have been the Lead’s original Legacy Id.
    
    Scenario: Email not on existing Contact, on multiple existing leads = Contact not created
    Given I'm on SiteCore QA
  	And I navigate to a lead form
  	And I set the lead form email address to "cmr315LF1@mailinator.com"
  	And I submit a lead form
  	Then I see "Thank you" message after submission
  	And I navigate to another lead form
  	And I set the lead form email address to "cmr315LF1@mailinator.com"
  	And I submit a lead form
  	Then I see "Thank you" message after submission
  	And I launch postman
  	And I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the email field to "cmr315LF1@mailinator.com"
    Then I receive a response with StatusCode: 400
    And the ReturnMessage: "Multiple leads were found with this Email."

    Scenario: Verify thgat missing email throws 400 status code
    Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the email address field to ""
    Then I see StatusCode: 400
    And I see the ReturnMessage: “Contact already exists with this Email.”
    
    Scenario: Verify that missing CorrelationId while creating a user throws 400 status code
    Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the correlationId field to ""
    Then I see StatusCode: 400
    And I see the ReturnMessage: "Required Fields Missing.”
    
Scenario: Verify that missing FirstName while creating a user throws 400 status code
    Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the FirstName field to ""
    Then I see StatusCode: 400
    And I see the ReturnMessage: "Required Fields Missing.”
    
Scenario: Verify that missing LastName while creating a user throws 400 status code
    Given I execute a POST of User Sync - Create User 
    And I have the latest payload for the above POST
    And I set the LastName field to ""
    Then I see StatusCode: 400
    And I see the ReturnMessage: "Required Fields Missing.”
    


