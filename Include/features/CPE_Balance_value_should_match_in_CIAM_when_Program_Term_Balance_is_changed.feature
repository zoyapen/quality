Feature: CPE Balance value should match in CIAM when Program Term Balance is changed

  Scenario: Submit New CPE changing the Active Program Term value
    Given Contact with active Program Tern
    And has "Additional Maintenance Credits Required" still need to be completed
    When Contact submit new CPE credit via CPE portal
    And CPE credit value is Accpted as an Applied CPE
    Then Contact record should be sent to the Bus with the correct "CPEBalance" value
    
    
