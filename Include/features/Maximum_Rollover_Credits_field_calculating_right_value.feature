Feature: Eligible Rollover Credits should be calculated correctly
  Eligible Rollover Credits are counted once it goes beyond the minimum total CPEs requirement and are dependent on type of CPE, activity date and should not go over Maximum Rollover Credits

@AMS-2077

  Scenario: Member satisfied minimum CPE requirement and submits Group A CPE within last 6 months of term end date
    Given member has an active maintenance term
    And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    When member submits Group A CPE through portal
    And CPE activity end date is within last 6 months of program term end date
    Then CPE is evaluated for Rollover
    And 'Eligible Rollover Credits' field is incremented by added Group A CPE value
    
  Scenario: Member satisfied minimum CPE requirement and submits Group B CPE within last 6 months of the term end date
    Given member has an active maintenance term
    And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    When member submits Group B CPE through portal
     And CPE activity end date is within last 6 months of program term end date
    Then CPE is ineligible for rollover
    And 'Eligible Rollover Credits' field remains unchanged
    
  Scenario Outline: Member has satisfied minimum CPE requirement and submits Group A CPE 7 months before the term end date
    Given member has an active maintenance term
    And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    When member submits <CPEType> CPE through portal
    And CPE activity end date is before 6 months of program term end date
    Then CPE is ineligible for rollover
    And 'Eligible Rollover Credits' field remains unchanged
  
   Examples:
     |CPEType|
     |Group A|
     |Group B|
    
  Scenario Outline: Member satisfied minimum CPE requirement and submits Group A CPE at term end date and goes beyond max Rollover credits
    Given member has an active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    And 'Maximum Rollover Credits' field on Program Term page is <MaximumCPEsAllowed>
    And 'Eligible Rollover Credits' field on Program Term page is <EligibleCredits>
    When member submits Group A CPE with 10 credits through portal
    And CPE activity end date is within 6 months of program term end date
    Then CPE is ineligible for rollover
    And 'Eligible Rollover Credits' field remains unchanged
    
    Examples:
      |Program|MaximumCPEsAllowed|EligibleCredits|
      | CISSP |     40           |      40       |
      | CSSLP |     30           |      30       |
      | CCSP  |     30           |      30       |
      | SSCP  |     20           |      20       |
      | CAP   |     20           |      20       |
      | HCISPP|     20           |      20       |
  
  Scenario: Member submits group A CPEs till it goes beyond max Rollover credits
  Scenario Outline: Member has satisfied minimum CPE requirement and submits Group A CPE at term end date and goes beyond max Rollover credits
    Given member has an active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    And 'Maximum Rollover Credits' field on Program Term page is <MaximumCPEsAllowed>
    And 'Eligible Rollover Credits' field on Program Term page is <EligibleCredits>
    When member submits Group A CPE with 10 credits through portal
    And CPE activity end date is within 6 months of program term end date
    Then partial CPE is eligible for rollover
    And 'Eligible Rollover Credits' field increments by 2 
    
    Examples:
      |Program|MaximumCPEsAllowed|EligibleCredits|
      | CISSP |     40           |      38       |
      | CSSLP |     30           |      28       |
      | CCSP  |     30           |      28       |
      | SSCP  |     20           |      18       |
      | CAP   |     20           |      18       |
      | HCISPP|     20           |      18       |
      
  Scenario Outline: Member has outstanding total CPE requirement and submits CPE within last 6 months of the term end date
    Given member has an active maintenance term
    And Group A credits earned is less than Group A minimum
	And Total Eligible Credits is less than Total CPE Minimum
    When member submits <GroupType> CPE through portal
    And CPE activity date is within 6 months of program term end date
    Then CPE is ineligible for rollover
    And 'Eligible Rollover Credits' field remains unchanged
 
   Examples:
          |GroupType|
          | Group A |
          | Group B |
  
  
  Scenario: Member submits group A CPE in large chunk
  Scenario Outline: Member has outstanding total CPE requirement and submits large chunk of Group A CPE at the end of the term which satisfies total CPEs goal
   Given member has an active maintenance term for <Program>
   And Group A credits earned is less than Group A minimum
   And Total Eligible Credits is less than Total CPE Minimum
   And 'Group A Minimum' is <GroupAMinimum>
   And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
   And 'Group A credits' field is <GroupAEarned>
   And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
   When member submits Group A CPE with 40 credits through portal
   And CPE activity date is within 6 months of program term end date
   Then partial CPE is eligible for rollover
   And 'Eligible Rollover Credits' field value is <EligibleRolloverCredits>
    
    Examples:
      |Program| GroupAMinimum  | TotalCpeMinimum  | GroupAEarned | TotalEligibleCredits|EligibleRolloverCredits|
      | CISSP |     90         |    120           |     70       |       100           |        20             |
      | CSSLP |     60         |    90            |     60       |       80            |        30             |
      | CCSP  |     60         |    90            |     60       |       80            |        30             |
      | SSCP  |     45         |    60            |     40       |       50            |        30             |
      | CAP   |     45         |    60            |     40       |       50            |        30             |
      | HCISPP|     45         |    60            |     40       |       50            |        30             |


  Scenario: Member in grace period submits group A CPE with activity date within term dates
  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date within term dates and goes beyond minimum CPE requriement
    Given member is outside of active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
    And Total Eligible Credits is less than Total CPE Minimum
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And group A, total CPEs goal are marked as complete
    When member submits 5 credits Group A CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Eligible Rollover Credits' field gets incremented by 5

   Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |


  Scenario: Member in grace period submits group B CPE with activity date within term dates
  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date within term dates and goes beyond minimum CPE requriement
    Given member is outside of active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
    And Total Eligible Credits is less than Total CPE Minimum
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Total CPE' goal is marked complete
    When member submits 5 credits Group B CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged

    Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |
 
  Scenario: Member in grace period submits group A CPE with activity date outside of term dates
  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date outside of term dates and goes beyond minimum CPE requirement
    Given member is outside of active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
    And Total Eligible Credits is less than Total CPE Minimum
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And group A, total CPEs goal are marked as complete
    When member submits 5 credits Group A CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Eligible Rollover Credits' field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date beyond term end date
    Then 'Total Eligible Credits' field remains unchanged
    And 'Eligible Rollover Credits' field remains unchanged

   Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |

  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date outside of term dates and goes beyond minimum CPE requriement
    Given member is outside of active maintenance term for <Program>
    And Group A credits earned is more than Group A minimum
    And Total Eligible Credits is less than Total CPE Minimum
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Total CPE' goal marked complete
    When member submits 5 credits Group B CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group B CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Eligible Rollover Credits' field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date beyond term end date
    Then 'Total Eligible Credits' field remains unchanged
    And 'Eligible Rollover Credits' field remains unchanged

   Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |