Feature: Customer able to update the information in profile   
  As a ISC2 customer, once I log in, I should be able to see my profile and also make changes if required.

@www-2730
  Scenario Outline: Customer willing to modify profile without signin
    Given customer is in <CustomerType> status
    When navigates to ISC2 homepage
    And skips the log in functionality
    Then unable to access my profile page
  
  Examples:
    | CustomerType |
    | Member       |
    | Candidate    |
    
  Scenario Outline: Customer willing to modify profile after signin  
    Given customer is in <CustomerType> status
    When navigates to ISC2 homepage
    And completes login functionality
    Then able to access my profile page
    And customer allowed to edit fields
    But <AccessLevel> to edit First name, last name and middle name
    And primary email cannot be edited
    And work address not present on page layout
    And changes made in sitecore are visible in Salesforce, LMS, CRM, and Okta
    
    Examples:
    | CustomerType | AccessLevel |
    | Member       | Denied      |
    | Candidate    | Denied      |

  Scenario Outline: Customer edits profile with a matching Billing and Mailing address
    Given Customer BillingAddress and Mailing Address match
    Then Same As Mailing Address checkbox should be checked
    
Scenario Outline: Customer edits profile with a Billing that is not the same as their Mailing address
    Given Customer BillingAddress and Mailing Address do not match
    Then Same As Mailing Address checkbox should not be checked
    