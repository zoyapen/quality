Feature: when a boot camp class product gets assigned to an instructor

  Scenario: when a boot camp class product gets assigned to an instructor, it should pull the instructor fee based on the instructor compensation
    Given A B2B Quote is created and a CCSP Boot Camp Bundle is added
    And Ship by date and event date is more than 30 days in advance on the fulfillment editor
    And Quote is submitted for approval
    When Finance approval and fulfillment indicators turns green
    Then The quote is approved
    And Docusign email is sent to the customer
    And Customer completes docusign
    When Opportunity Stage turns into Agreement signed
    Then A new Order is created in draft
    And navigate to the order and approval history
    When Order is approved
    Then Order is active and a fulfillment request is created on the opportunity related list
    And user is on the fulfillment request
    When user navigates to related list
    Then A Instructor request is created
    And on the Instructor request user clicks on Instructor Assignment
    When the instructor is assigned
    Then the instrctor fee is pulled based on the instructor compensation table for Boot camp instructor
    
    
  Scenario: when a non boot camp class product gets assigned to an instructor, it should pull the instructor fee based on the instructor compensation
    Given A B2B Quote is created and a CCSP CBK Instructor-Led Training from In Person class is added
    And Ship by date and event date is more than 30 days in advance on the fulfillment editor
    And Quote is submitted for approval
    When Finance approval and fulfillment indicators turns green
    Then The quote is approved
    And Docusign email is sent to the customer
    And Customer completes docusign
    When Opportunity Stage turns into Agreement signed
    Then A new Order is created in draft
    And navigate to the order and approval history
    When Order is approved
    Then Order is active and a fulfillment request is created on the opportunity related list
    And user is on the fulfillment request
    When user navigates to related list
    Then A Instructor request is created
    And on the Instructor request user clicks on Instructor Assignment
    When the instructor is assigned
    Then the instrctor fee is pulled based on the instructor compensation table for non Boot camp instructor