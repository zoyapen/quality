Feature: when Senior Education and Training Manager with education delivery specialist profile and the Salesforce CPQ Custom permission

  Scenario:when Senior Education and Training Manager with education delivery specialist profile and the Salesforce CPQ Custom permission set approve a quote that is rush approved should move to status approved and primary should be checked
    Given a quote is created with a Book product
    And the fulfillment information is filled in with ship by date less than 14 days
    And the Quote is submitted for approval as a salers user
    When The quote is in approval process, login as RMD and approve Rush approval level 1
    And now login as a Senior education and training manager with Education delivery specialist profile and rush approve level 2
    Then The quote status is approved and Primary field is checked
