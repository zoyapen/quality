Feature: Payment of back dues and Reinstatement fee after suspension
  As a member services staff, I should be able to process the back dues and reinstatement fees in Salesforce

  Scenario: Processing back dues and reinstatement fee for contact with a subscription renewal
    Given Member is suspended
    And has subscription & program profile in suspended status
    When member services renew the subscription
    And create new sales order with new sales order line for each year of unpaid AMF and reinstatement fee
    Then navigated to shopping cart with correct back dues and reinstatement fee
    When click on payment button
    Then subscription is changed to pending status 
    And extended for next one year
    
  Scenario: Processing back dues and reinstatement fee for contact without a subscription renewal
    Given Member owe back dues AMF for multiple years
    And has no subscription in Salesforce
    And has program profile in suspended status
    When member services create new Sales Order and Sales order line
    And pay for owed backdues for one term through shopping cart
    Then new subscription back dated in past gets created
    When create new sales order with new sales order line for each year of unpaid AMF and reinstatement fee
    And navigate to shopping cart with correct total back dues and reinstatement fee
    And click on payment button
    Then back dated subscription is changed to active status 
    And extended for next one year
