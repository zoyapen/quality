Feature: Member able to pay the AMF using Fonteva Shopping chart 
As a member when I go to the AMF shopping cart check out page, I should see specific amount based on my membership.

@AMS-2038

  Scenario Outline: Customer logins to the Member Payment Gateway page
 Given a contact has a sales order 
 And a Sales line Order with Item <MemberFees>
 And sales order id 
 When navigated to Test shopping cart URL 
 And change the sales order id based on current sales order 
 Then the shopping cart displayed is for current sales order 
 And the correct AMF amount displayed should be <Amount>
 And Payment can be done with a credit card
 When payment details are submitted 
 Then Thank you confirmation receipt should be displayed
 
 Examples:
   |MemberFees| Amount|
   |Professional Membership - Annual Fee | $125|
   |Associate Membership - Annual Fee | $50|