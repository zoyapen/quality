Feature: Send receipt button on receipt page layout
As member services staff, I should be able to send receipt to the contact's email address by clicking on send receipt button

Scenario: Member has already paid AMF
 Given member has paid AMF
 When member services user navigate to receipt object
 And click on Send Receipt button
 Then receipt is emailed to member on address mentioned on contact page
 And activity is recorded in Activity history related list