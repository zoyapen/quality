Feature: Verify that when MulesoftContactServiceFeature flag is set to false - creating account via sitecore shows up contact in service bus

  Scenario: Verify that when MulesoftContactServiceFeature flag is set to false - creating account via sitecore shows up contact in service bus
    Given I'm on Sitecore
    And I create an account
    Then I see the above account is created successfully
    When I navigate to service bus
    Then I see the above contact details in the service bus
