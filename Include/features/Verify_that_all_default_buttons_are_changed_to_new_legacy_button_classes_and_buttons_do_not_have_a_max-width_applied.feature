Feature: Verify that all default buttons are changed to new legacy button classes and buttons do not have a max-width applied

  Scenario Outline: Check whether default buttons are changed to new legacy button classes
    Given I'm on <pageName>
    When I inspect the default buttons
    Then I see the default buttons are changed to "button--legacy-default"
    Examples:
      |pageName       |
      |resetPassword  |
      |siginiinAccount|
      |formBanner     |
      |sponsorship    |
    

  Scenario Outline: Check whether the buttons do not have a max-width applied
    Given I'm on <pageName>
    When I inspect the buttons
    Then I do not see the max width property for any of the buttons
    Examples:
      |pageName             |
      |resetPassword        |
      |siginiinAccount      |
      |formBanner           |
      |sponsorship          |
      |certification        |
      |preferenceCenter     |
      |trainingSearchResults|
      |trainingSearch       |

  