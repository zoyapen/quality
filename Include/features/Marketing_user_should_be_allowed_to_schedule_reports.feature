Feature: Marketing user should be allowed to schedule reports

    Scenario Outline: Verify that a marketing user should be allowed to schedule reports
        Given I'm logged into Salesforce
        And I impersonate a marketing user
        When I schedule a <report>
        Then I receive the <report> via email
            Examples:
              |report                                   |
              |EMEA Int in Individual Training Contacts |
              |EMEA B2B Training Leads Opt Out          |
              |EMEA Enterprise PrivateOnSite Contacts   |
            