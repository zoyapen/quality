Feature: Update the payment due mesage to show 'Pay now to upgrade to a professional membership' when membership is due for an upgrade

  Scenario: Update the payment due day when membership is due for an upgrade
    Given member who has Associate membership
    And have an endorsement request with a status membership complete
    And Today's date must be more than 45 days before the Subscription Paid Through Date
    When member navigates to the dashboard
    Then Maintenance Fee Tile should read 'Pay now to upgrade to a professional membership'
