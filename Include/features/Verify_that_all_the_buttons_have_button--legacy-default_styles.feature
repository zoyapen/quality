Feature: Verify that all the buttons have button--legacy-default styles

  Scenario Outline: Check whether the buttons have button--legacy-default styles
    Given I navigate to <page>
    When I inspect the buttons
    Then I see the buttons have button--legacy-default styles
    And the buttons are left aligned
    Examples:
        |page                      |
        |exam-development          |
        |member-development-program|
        
