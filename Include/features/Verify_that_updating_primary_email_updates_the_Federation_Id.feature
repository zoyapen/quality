Feature: Verify that updating primary email updates the Federation Id 

Scenario: Verify that updating primary email updates the Federation Id
    Given I log into SF
    And I navigate to contacts 
    And I edit the above contact
    And I set the primary email to "qa1CMR-202@mailinator.com"
    And I save the above primary email
    Then I see the above primary email is updated to "qa1CMR-202@mailinator.com"
    And I navigate to setup
    And I open users
    And I search for the above email
    Then I see the federation id and email is "qaCMR-202@mailinator.com"