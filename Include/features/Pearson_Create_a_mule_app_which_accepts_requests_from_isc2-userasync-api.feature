Feature:Pearson Create a mule app which accepts requests from isc2-userasync-api
  <isc2-userasync-api, transforms them, and sends data to Pearson to create and merge users and return pvue id to isc2-userasync-api>

  Scenario: GET Ping for Pearson API
    Given End Point for RESTful API is used
    When "Ping" session is Sent
    Then "resultsStatus" equals "success"
    And "message" equals "null"
    And "env" equals "CTT"
    And Status "200"
    
    Scenario: GET PingDB for Pearson API
    Given End Point for RESTful API is used
    When "PingDB" session is Sent
    Then "resultsStatus" equals "success"
    And "message" equals "null"
    And "env" equals "CTT"
    And Status "200"
    
    Scenario: POST CDD for Pearson API
    Given End Point for RESTful API is used
    When "CDD" session is Sent
    Then "candidateID" equals "candidateIDNumber"
    And "clientCandidateID" equals "clientCandidateIDNumber"
    And "status" equals "Accepted"
    And "message" equals "null"
    And "date" equals "DatesGMT"
    And Status "200"
