Feature: Customer receives an email when account created
  As a customer, I should be able to receive email when I create account for the first time

@AMS-1781

  Scenario: Contact gets created in Salesforce
    Given customer sign up
    When contact is created in Salesforce
    Then account creation email is sent to their primary email
