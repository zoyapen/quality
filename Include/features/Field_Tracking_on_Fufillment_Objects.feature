Feature:Field Tracking on Fufillment Objects
  As an EDU professional or SysAdmin, I want visibility into what changes have been made to fulfilment fields after it has been modified 

Scenario Outline: Modifying field(s) on Fulfillment Request object
    Given the education professional is on Fulfillment Request record
	When the <FufillmentRequestField> is updated
	Then the education professional can see the change history
    
    Examples:
      |FufillmentRequestField|
      | Account__c|
      | B2C_Class_Product__c|
      | B2C_Seats__c|
      | Child_Request_Fulfilled__c|
      | Child_Request_In_Progress__c|
      | End_Date__c|
      | Event_City__c|
      | Ev ent_Country__c|
      | Location_City__c|
      | Location_Country__c|
      | No_of_Attendees__c|
	  | Opportunity__c|
	  | Requested_Instructor__c|
	  | Ship_by_Date__c|
	  | Shipping_City__c|
	  | Shipping_Country__c|
	  | Shipping_POC__c|
      | Start_Date__c|

Scenario Outline: Modifying field(s) on Material Request object
	Given the education professional is on Material Request record
	When the <MaterialRequestField> is updated
	Then the education professional can see the change history 
    
    Examples: 
      |Material Request|
      | Shipping POC|
      | Ship By Date|
      | Start Date|
      | Status|
      | EIN|
      | Product|
      | Quantity|
      | UPS Books Tracking Number|
      | Fedex Books Tracking Number|
      | eBook access code|
      | # of Books Shipped|
      | Books Shipped?|
      | Account Name|
      | Shipping Street|
      | Shipping City|
      | Shipping State/Province|
      | Shipping Country|
      | Shipping Zip/Postal Code|
      | Shipping Company|
  
  Scenario Outline: Modifying field(s) on Instructor Request object
	Given thr education professional is on Instructor Request record
	When the <InstructorRequestField> is updated
	Then the education professional can see the change history
    
    Examples:
      
      |Instructor Request|
      | Instructor Fee|
      | Notes for Instructor|
      | Certification Required of Instructor|
      | Product|
      | Related Class Product|
      | Requested Instructor|
      | Instructor Assigned|
      | Start Date|
      | Location|
      | Location Company|
      | Location Type|
      | Location POC|
      | Location Street|
      | Location City|
      | Location State/Province|
      | Location Country|
      | Location Zip Code|
      | Subject|
      | Online Class Indicator|
    
Scenario Outline: Modifying field(s) on Class Roster object
	Given the education professional is on Class Roster object
	When the <ClassRosterRequestField> is updated
	Then the education professional can see the change history
    
    Examples:
      |Class Roster|
      | Account Name|
      | Status|
      | Product|
      | Seats|
      | Start Date|

Scenario Outline: Modifying field(s) on Venue Request object
	Given the education professional is on Venue Request record
	When the <VenueRequestField> is updated
	Then the education professional can see the change history 
    
    Examples:
      |Venue Request|
      | Product|
      | B2C Class|
      | B2C Product|
      | B2C Seats|
      | Quantity|
      | Internal Comments|
      | Venue|
      | Projector Needed|
      | Website|
      | Start Date|
      | End Date|
      | Start Time|
      | End Time|
      | Timezone|
     
