Feature: Submit CPEs when in grace period 
As a member, when I am in grace period, I am allowed to submit CPEs through portal but it will get accepeted only when activity date is within program term start and end date.

@AMS-1755

  Scenario: Member in grace period submitting CPE within term dates through portal
  Given current date has passed maintenance term end date
  And is within 90 days from term end date
  When member submits CPE dated within program term dates through portal
  Then CPE is visible as "Accepted" in Salesforce 

  Scenario: Member in grace period submitting CPE outside of term dates through portal
  Given current date has passed maintenance term end date
  And is within 90 days from term end date
  When member submits CPE dated outside of program term dates through portal
  Then CPE is visible as "Rejected - Passed Program Term" in Salesforce