Feature:Create and update interfaces for sitecore contact

  Scenario: Verify that creating a new account in the sitecore displays correct data in salesorce
    Given I'm on Sitecore "Registration" page
    When I submit request for new account
    Then I see a contact created for the above account
    And Salesforce has the correct data for that newly created record

  Scenario: Update Profile data via ISC2 site
    Given account has already been created
    Given I Sign In using my ISC2 account
    And navigate to Profile page
    When I edit any Profile data
    And Save
    Then my edit data is sent to Salesforce
    And matches the edits made via ISC2 site
    
    Scenario: Update Profile data via Salesforce
    Given account has already been created
    Given I Sign In using my ISC2 account
    And navigate to Profile page
    When I edit any Profile data
    And Save
    Then my edit data is sent to Salesforce
    And matches the edits made via ISC2 site
  
