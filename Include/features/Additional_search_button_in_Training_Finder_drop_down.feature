Feature: Additional search button in Training Finder drop down
 

  Scenario: make search selection using various Combinations
    Given searcher has navigated to "https://wwwqa.isc2.org/trainingsearchresult"
    When searcher clicks on "Advance Filters"
     And making selections using various Combinations with <advanceSearchFields>
     And clicking "Search" botton at the bottom of the window listed next to "Clear Filters"
    Then "Advance Filters" window close
     And combinations selected displays "Training Schedule" based on the criteria 
  

 Scenario: clearing Advance Search data
    Given searcher has navigated to " https://wwwqa.isc2.org/trainingsearchresult"
    When searcher clicks on "Advance Filters"
     And making selections using various Combinations with <advanceSearchFields>
     And clicking "Clear Filters" to removed data
     And clicking "Search" button at the bottom of the window listed next to "Clear Filters"
    Then "Advance Filters" window close
     And displays the full list of "Training Schedule" data
     And combinations selected displays "Training Schedule" based on the criteria 
     
    |advanceSearchFields|
    | Certiication |
    | Classroom-based |
    | Training Provider |
    | StartDate |
    | EndDate |
    | City |
    | Country/Region |
    | State/Province |