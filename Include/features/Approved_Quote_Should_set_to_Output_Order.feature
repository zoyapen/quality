Feature: Once a Quote is Approved, the Quote output Should be set Order and if the quote is moved back to draft the output type should be quote

  Scenario: Quote output type is updated based on the quote status
    Given A Quote is created for a B2B opportunity
    And A Voucher Product is added and submiited for Approval
    When Quote is approved
    Then Quote output type is changed to Order
    And PP is changed to Terminated status
    Then Quote output types is changed to Quote
