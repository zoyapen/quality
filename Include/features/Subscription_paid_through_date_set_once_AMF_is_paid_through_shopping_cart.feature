Feature: Subscription paid through date set once AMF is paid through shopping cart
As a Member Services Staff, after an AMF payment is made through the Fonteva shopping cart (or back office), I need the Subscription that gets created/extended to have a subscription paid through date matching the End Date of the subscription

  Scenario: Customer pays AMF for becoming member
    Given customer has active candidacy term
    And has only membership goal incomplete
    When pay outstanding AMF through shopping cart
    Then new subscription is created
    And subscription paid through date matches subscription end date
    
  Scenario: Member pays AMF for next year before current subscription end date
    Given member has active subscription
    And current date is within 45 days of subscription end date
    When pay outstanding AMF through shopping cart
    Then subscription end date is extended for next one year
    And subscription paid through date matches subscription end date
    
  Scenario: Member pays AMF once in grace period
    Given member is in grace period
    And current date is within 90 days of passing subscription end date
    And subscription paid through date matches subscription end date
    When invoice is created
    Then subscription end date is extended for next one year
    But subscription paid through date field is not updated 
    When member pay invoiced AMF through shopping cart
    Then subscription end date remains the same
    And subscription paid through date matches subscription end date
    
  Scenario: Member doesn't pay AMF till end of grace period
    Given member is in grace period
    And invoice is created
    And subscription end date is extended for next one year
    And subscription paid through date is less than subscription end date
    When member is out of grace period 
    And no AMF is paid against invoice
    Then subscription is changed to suspended status 
    And subscription paid through date field is not updated
