Feature: Mac Safari Training Finder Links able to use Learn More
  

  Scenario: Clicking Learn More from Training Finder navigate to training with more info
    Given Training Search page "https://wwwqa.isc2.org/Training" has list of training events
    When any of the "Learn More" training bottons are click
    Then the page with information for the training displays
    
    
