Feature:As a Member or Candidate, I want to clearly see in the Web Dashboard the dates that will be covered by my AMF payment so that I know exactly what I am paying for

  Scenario: As a Member, I want to clearly see in the Web Dashboard the dates that will be covered by my AMF payment so that I know exactly what I am paying for
    Given A member has an subscription
    And Member has already paid the Payment and there are no dues as of today
    And member does not have any endorsement request as passed in the last 9 months
    When Member navigates to the member dashboard
    Then The member should see the dates the member has paid the membership
  Scenario: As a Candidate, I want to clearly see in the Web Dashboard the dates that will be covered by my AMF payment so that I know exactly what I am paying for
    Given A Candidate has an subscription
    And Candidate has already paid the Payment and there are no dues as of today
    When Candidate navigates to the member dashboard
    Then The Candidate should see Member dues component that should display New 1 Year Annual Maitenance Fee