Feature: Fonteva Shopping cart updated to label with Annual Maintenance Fee
  As a customer, when I navigate to shopping cart I should see Annual Maintenance Fee label

  Scenario: Membership dues visible on Dashboard
    Given member login to Dashboard
    And has membership due
    When click on pay button
    Then navigates to shopping cart
    And see Annual Maintenance Fee label
    When payment is done through credit card
    Then receipt is generated
    And Annual Maintenance Fee is indicated on it 
