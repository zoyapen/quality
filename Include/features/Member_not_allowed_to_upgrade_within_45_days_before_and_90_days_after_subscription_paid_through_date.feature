 Feature: Member not allowed to upgrade within 45 days before and 90 days after subscription paid through date
  As an Associate I should be able to upgrade before 45 days from subscription end date and once I enter 45 day window, should be asked for renewal before I can pay for upgrade.    
  
  Scenario: Associate willing to upgrade before 45 days prior to subscription term end date
    Given A user has an Active subscription
    And ItemName must be Associate Membership - Annual Fee
    And Today's date must be more than 45 days prior to the SubscriptionPaidThroughDate
    And Must have endorsement request with RequestStatus as Membership Complete within the last 9 months
    When User navigates to the membership dues page
    Then user should be allowed to pay upgrade fee of $75
  
  Scenario: Associate willing to upgrade within 45 days of subscription term end date
    Given A user has an Active subscription
    And ItemName must be Associate Membership - Annual Fee
    And Today's date must be less than 45 days prior to the SubscriptionPaidThroughDate
    And Must have endorsement request with RequestStatus as Membership Complete within the last 9 months
    When User navigates to the membership dues page
    Then user should not be allowed to pay upgrade fee of $75
    
  Scenario: Associate willing to upgrade 90 days after subscription term end date
    Given A user has an Active subscription
    And ItemName must be Associate Membership - Annual Fee
    And Today's date passed the SubscriptionPaidThroughDate but no more than 90 days
    And Must have endorsement request with RequestStatus as Membership Complete within the last 9 months
    When User navigates to the membership dues page
    Then user should not be allowed to pay upgrade fee of $75