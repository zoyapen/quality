Feature:Start and End Time Required on Fulfillment Editor

  Scenario:
    Given User has to enter the Start time and End time on the fulfillment editor for the Indicator to turn green
    And Edit Fulfillment information
    When Start time and End time and other required information is entered
    Then fulfillment indicator on the quote turns Green
