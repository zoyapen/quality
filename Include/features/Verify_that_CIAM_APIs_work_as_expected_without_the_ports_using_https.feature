Feature: Verify that CIAM APIs work as expected without the ports using https

 Scenario Outline: Check whether CIAM APIs work as expected without the ports using https
    Given I execute a POST for <api>
    Then I see correct response for the above api
    When I GET a <user> by email 
    Then I see the user details 
     Examples:
      |api                                 |
      |Common Services - EmailNotification |
      |D2L - Create User                   |
      |D2L - Create User                   |
      |Okta - Create User                  |
      |Pearson - CDD                       |
      |SF - Create Contact                 |
      |UserSync - Create User              |

 Scenario Outline: Check whether CIAM APIs work as expected without the ports using https
    Given I execute a PUT for <api>
    Then I see correct response for the above api
     Examples:
      |api                                 |
      |SF - Update System Status           |

 Scenario Outline: Check whether CIAM APIs work as expected without the ports using https
    Given I execute a GET for <api>
    Then I see correct response for the above api
     Examples:
      |api                                 |
      |UserAsync - Ping Service            |