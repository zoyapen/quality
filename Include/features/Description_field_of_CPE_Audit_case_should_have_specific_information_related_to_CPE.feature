Feature: Description field of CPE Audit case should have specific information related to CPE
  As a Customer support specialist, I should be able to see details like activity dates, CPE category, CPE Type, detail under CPE types (come be hours or credits)

@AMS-1964

  Scenario: Submitted CPE gets selected for audit
    Given member submits CPE through portal
    And gets selected for audit
    When CPE specialist navigates to CPE Audit case to resolve the CPE
    Then description field has information such as activity dates, CPE category, CPE Type, and hours/credits
    
