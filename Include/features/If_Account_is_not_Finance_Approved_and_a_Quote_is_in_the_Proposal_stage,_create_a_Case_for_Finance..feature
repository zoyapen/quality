Feature: If Account is not Finance Approved and a Quote is in the Proposal stage, create a Case for Finance.

  Scenario: If the account is not finance approved create a case for Finance when a quote is submitted for approval and is in Proposal State
    Given An Account is created that is not Finance Approved
    And Product is added and fulfillment information is filled in
    And Submit the Quote for Approval so quote is in Proposal status
    When AR Action Queue is selected from Cases
    Then There should be a new Case at the bottom of the list. The subject should be Finance Check Needed - Quote in Proposal
