Feature: Rollover CPE credits created and applied to next eligible maintenance term
Any Group A CPEs accepted over the Total Group A minimum and beyond the total CPE requirement with CPE end date within last 6 months of the cycle can roll over and the limit is maximum rollover credits available per program. 

@AMS-2073

  Scenario: Member satisfied minimum requirement and submits Group A CPE within last 6 months of term end date
    Given member has an active maintenance term
    And group A, total CPE goal are marked as complete
    When member submits Group A CPE through portal
    And CPE activity end date is within last 6 months of program term end date
	And Group A credits earned is more than Group A minimum
	And Total Eligible Credits is more than Total CPE Minimum
    Then Applied CPE has status of Accepted
	And Evaluate For Rollover checkbox is marked true
    And 'Eligible Rollover Credits' field is incremented by added Group A CPE value beyond minimum group A CPEs
	When new maintenance term is issued to same program profile
	Then CPE with record type 'CPE Rollover' with credits equal to 'Eligible Rollover Credits' is applied to the new term
    
  Scenario Outline: Member satisfied minimum CPE requirement and submits Group A CPE at term end date and goes beyond max Rollover credits
    Given member has an active maintenance term for <Program>
    And group A, total CPEs goal are marked as complete
    And 'Maximum Rollover Credits' field on Program Term page is <MaximumCPEsAllowed>
    And 'Eligible Rollover Credits' field on Program Term page is <EligibleCredits>
    When member submits Group A CPE with 10 credits through portal
    And CPE activity date is within 6 months of program term end date
    Then CPE is ineligible for rollover
    And 'Eligible Rollover Credits' field remains unchanged
	When new maintenance term is issued to same program profile
	Then CPE Rollover with credits equal to 'Eligible Rollover Credits' is applied to the new term
    
    Examples:
      |Program|MaximumCPEsAllowed|EligibleCredits|
      | CISSP |     40           |      40       |
      | CSSLP |     30           |      30       |
      | CCSP  |     30           |      30       |
      | SSCP  |     20           |      20       |
      | CAP   |     20           |      20       |
      | HCISPP|     20           |      20       |
  
  Scenario: Member submits group A CPEs till it goes beyond max Rollover credits
  Scenario Outline: Member satisfied minimum CPE requirement and submits Group A CPE at term end date and goes beyond max Rollover credits
    Given member has an active maintenance term for <Program>
    And group A, total CPEs goal are marked as complete
    And 'Maximum Rollover Credits' field on Program Term page is <MaximumCPEsAllowed>
    And 'Eligible Rollover Credits' field on Program Term page is <EligibleCredits>
    When member submits Group A CPE with 10 credits through portal
    And CPE activity date is within 6 months of program term end date
    Then partial CPE is eligible for rollover
    And 'Eligible Rollover Credits' field increments by 2
	When new maintenance term is issued to same program profile
	Then CPE Rollover with credits equal to 'Eligible Rollover Credits' is applied to the new term
    
    Examples:
      |Program|MaximumCPEsAllowed|EligibleCredits|
      | CISSP |     40           |      38       |
      | CSSLP |     30           |      28       |
      | CCSP  |     30           |      28       |
      | SSCP  |     20           |      18       |
      | CAP   |     20           |      18       |
      | HCISPP|     20           |      18       |
   
  Scenario: Member submits group A CPE in large chunk
  Scenario Outline: Member has outstanding total CPE requirement and submits large chunk of Group A CPE at the end of the term which satisfies total CPEs goal
   Given member has an active maintenance term for <Program>
   And group A, total CPEs goal are marked as incomplete
   And 'Group A Minimum' is <GroupAMinimum>
   And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
   And 'Group A credits' field is <GroupAEarned>
   And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
   When member submits Group A CPE with 40 credits through portal
   And CPE activity date is within 6 months of program term end date
   Then partial CPE is eligible for rollover
   And 'Eligible Rollover Credits' field value is <EligibleRolloverCredits>
   When new maintenance term is issued to same program profile
   Then CPE Rollover with credits equal to 'Eligible Rollover Credits' is applied to the new term
    
     Examples:
      |Program| GroupAMinimum  | TotalCpeMinimum  | GroupAEarned | TotalEligibleCredits|EligibleRolloverCredits|
      | CISSP |     90         |    120           |     70       |       100           |        20             |
      | CSSLP |     60         |    90            |     60       |       80            |        30             |
      | CCSP  |     60         |    90            |     60       |       80            |        30             |
      | SSCP  |     45         |    60            |     40       |       50            |        30             |
      | CAP   |     45         |    60            |     40       |       50            |        30             |
      | HCISPP|     45         |    60            |     40       |       50            |        30             |

  Scenario: Member in grace period submits group A CPE with activity end date within last 6 months of term end date
  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date within term dates and goes beyond minimum CPE requriement
    Given member is outside of active maintenance term for <Program>
    And group A credits earned is more than group A minimum
    And total CPEs goal is marked incomplete
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And group A, total CPEs goal are marked as complete
    When member submits 5 credits Group A CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Eligible Rollover Credits' field gets incremented by 5
	When new maintenance term is issued to same program profile
	Then CPE Rollover with credits equal to 'Eligible Rollover Credits' is applied to the new term

   Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |
      
  Scenario: Member in grace period submits group A CPE with activity date outside of term dates
  Scenario Outline: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date outside of term dates and goes beyond minimum CPE requriement
    Given member is outside of active maintenance term for <Program>
    And group A credits earned is more than group A minimum
    And total CPEs goal is marked incomplete
    And 'Total CPE Minimum' field on Program Term page is <TotalCpeMinimum>
    And 'Total Eligible Credits' field on Program Term page is <TotalEligibleCredits>
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And group A, total CPEs goal are marked as complete
    When member submits 5 credits Group A CPE through portal with activity end date before last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    But 'Eligible Rollover Credits' field remains unchanged
    When member submits 5 credits Group A CPE through portal with activity end date within last 6 months of term end date
    Then Total Eligible Credits field gets incremented by 5
    And 'Eligible Rollover Credits' field gets incremented by 5
    When member submits 5 credits Group A CPE through portal with activity end date beyond term end date
    Then 'Total Eligible Credits' field remains unchanged
    And 'Eligible Rollover Credits' field remains unchanged
	When new maintenance term is issued to same program profile
	Then CPE Rollover with credits equal to 'Eligible Rollover Credits' is applied to the new term

   Examples:
      |Program| TotalCpeMinimum  |TotalEligibleCredits|
      | CISSP |     120          |      110           |
      | CSSLP |     90           |      80            |
      | CCSP  |     90           |      80            |
      | SSCP  |     60           |      50            |
      | CAP   |     60           |      50            |
      | HCISPP|     60           |      50            |