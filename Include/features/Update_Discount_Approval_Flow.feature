Feature: Update Discount Approval Flow for sales users in different regions

  Scenario Outline: Sales users in different <Regions> submit quote for approval with a discount range of 10% to 14% requires level 2 discount approval
    Given <Regions> Add a product to quote that can be discounted 
    And Sales user from <Regions> Applies a discount range of 10% to 14% to the product
    When the quote is submitted for Approval
    Then the discount approval goes to <Regions> level 2 user
   
    Examples:
        |Regions|
        | NAR   |
        | EMEA  |
        | APAC  |
        
    
    Scenario Outline: Sales users in different <Regions> submit quote for approval with a discount range of 15% to 19% requires a level 3 discount approval
        Given <Regions> Add a product to quote that can be discounted
        And Sales user from <Regions> Applies a discount range of 15% to 19% to the product
        When the quote is submitted for Approval
        Then the discount approval goes to <Regions> level 2 user
        When the discount is approved by <Regions> Level 2 user
        Then the discount approval goes to <Regions> level 3 user
   
    Examples:
        |Regions|
        | NAR   |
        | EMEA  |
        | APAC  |

    Scenario Outline: Sales users in different <Regions> submit quote for approval with a discount range of 20% or more requires a level 4 discount approval
        Given <Regions> Add a product to quote that can be discounted
        And Sales user from <Regions> Applies a discount range of 20% or more to the product
        When the quote is submitted for Approval
        Then the discount approval goes to <Regions> level 2 user
        When the discount is approved by <Regions> Level 2 user
        Then the discount approval goes to <Regions> level 3 user
        When the discount is approved by <Regions> Level 3 user
        Then the discount approval goes to <Regions> level 4 user
        
    Examples:
        |Regions|
        | NAR   |
        | EMEA  |
        | APAC  |      
    
    Scenario: LATAM Sales users submit a quote with a discount range of 1% or more then it is submitted for approval from Wesley Simpson
        Given LATAM sales user adds a product to quote that can be discounted
        And Sales user from LATAM Applies a discount range of 1% or more to the product
        When the quote is submitted for Approval
        Then the discount approval goes to Wesley Simson