Feature: Message is displayed in IE browser to inform users we no longer support IE 

  Scenario: Verify that message is displayed in IE browser to inform users we no longer support IE
    Given I open IE browser
    And I'm on Sitecore
    Then I see a message is displayed to inform users we no longer support IE
    