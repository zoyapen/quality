Feature:Sensitive ePayment fields are wiped off once payment is made against invoice
  As a system Admin, I shouldn't see any sensitive information stored in epayment record once member pays against invoice.

  Scenario: Member is in grace period
    Given member has AMF due
    And is in grace period
    When member navigates to dashboard
    And pays AMF against invoice through shopping cart
    Then new epayment record is created
    And sensitive fields on epayment records are wiped off
