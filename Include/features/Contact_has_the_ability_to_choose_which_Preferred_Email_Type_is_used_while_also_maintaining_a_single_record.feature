Feature: Contact has the ability to choose which Preferred Email Type is used while also maintaining a single record
  

  Scenario:
    Given Contact is active 
     And has a list of differnt emails for "Personal, Work, Other, and Assistant"
    When "Preferred Phone Type" is changed to something else
    Then Preferred Phone Type email is used in place of the previous
     And old record is "deleted" new one is "created"
     And contact is able to access all the systems using the changed "Preferred Phone Type"
    
