Feature: Verify that Long names and emails are displayed correctly on member profile page

Scenario Outline: Verify that Long names are displayed correctly on member profile page
    Given I navigate to sitecore QA environment
    And I create a new account
    And I enter the first name with <fCharacterLength>
    And I enter the last name with <lCharacterLength>
    And I enter all the necessary details
    And I create an account successfully
    Then I'm redirected to the landing page 
    When I navigate to member profile page
    Then I see the long name is displayed correctly 
    And does not run outside the frame
    Examples:
      |fCharacterLength  |lCharacterLength|
      |39                |1               |
      |40                |1               |
      |41                |1               |
      |49                |1               |
    
    Scenario Outline: Verify that Long emails are displayed correctly on member profile page
    Given I navigate to sitecore QA environment
    And I create a new account
    And I enter the primary email with <primaryEmailCharacterLength>
    And I enter all the necessary details
    And I create an account successfully
    Then I'm redirected to the landing page 
    When I navigate to member profile page
    Then I see the long name is displayed correctly 
    And does not run outside the frame
    Examples:
      |primaryEmailCharacterLength  |
      |40                           |
      |41                           |
      |42                           |
      |50                           |
