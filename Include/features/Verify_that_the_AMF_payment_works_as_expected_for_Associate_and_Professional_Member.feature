Feature: Verify that the AMF payment works as expected for Associate and Professional Member

 Scenario: Verify that an active Professional Member set subscription paid through date to 45 days in the future can reach the shopping cart and complete the payment
    Given I'm logged in as an active Professional Member
    And I navigate to the shopping cart
    When I attempt to pay with a credit card
    Then I should be abe to successfully complete the payment

 Scenario: Verify that an active Professional Member set subscription paid through date to 46 days in the future cannot reach the shopping cart and complete the payment
    Given I'm logged in as an active Professional Member
    When I navigate to the shopping cart
    Then the navigation should not be successful 
    And I should not seen an option to complete the payment
    
 Scenario: Verify that an active Associate Member set subscription paid through date to 45 days in the future can reach the shopping cart and complete the payment
    Given I'm logged in as an active Associate Member
    And I navigate to the shopping cart
    When I attempt to pay with a credit card
    Then I should be abe to successfully complete the payment
    
 Scenario: Verify that an active Associate Member set subscription paid through date to 46 days in the future cannot reach the shopping cart and complete the payment
    Given I'm logged in as an active Associate Member
    When I navigate to the shopping cart
    Then the navigation should not be successful 
    And I should not seen an option to complete the payment