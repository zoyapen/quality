Feature: Voucher code stated clearly how many characters are allowed 24-32 Character Code
 

  Scenario: AMF due payment and paid with Voucher statement shows how many charaters are allowed
    Given AMF paymnet is due
    When payment is being processed via "Maintenance Fee" page
    And payment method is "Voucher"
    Then "Voucher Code" displays the allowed amount of characters "24-32"
