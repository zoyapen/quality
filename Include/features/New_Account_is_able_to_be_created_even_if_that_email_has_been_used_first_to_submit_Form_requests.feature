Feature: New Account is able to be created even if that email has been used first to submit Form requests
  

  Scenario: Create New Account after submittion Form request using the same email
    Given I have submitted Form request from "http://integration.isc2.org/training/private-on-site"
     And using email "formRequestEmail@mailinator.com"
    When I access Sitecore to "Create New Account"
     And using email "formRequestEmail@mailinator.com" as part of the new account creation process
    Then I'm able to create the new account and access Sitecore after successful Sign In
     And Salesforce identifies my new account as "Contact"
