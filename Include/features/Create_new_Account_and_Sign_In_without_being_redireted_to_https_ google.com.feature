Feature: Create new Account and Sign In without being redireted to https google.com
  
# createNewsAccount:https://wwwqa.isc2.org/Sign-In/Registration?originalRelayState=https://google.com
  
  Scenario: Create new Account and Sign In
    Given vistor navigates to <createNewAccount>
    When all the required fields have been completed
        And the user clicks the button to "CREATE A NEW ACCOUNT"
    Then the new account will be created in Okta
        And navigate the account creator to ISC2 landing page