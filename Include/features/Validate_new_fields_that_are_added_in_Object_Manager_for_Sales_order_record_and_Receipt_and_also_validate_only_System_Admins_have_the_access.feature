Feature:Validate new fields that are added in Object Manager for Sales order record and Receipt and also validate only System Admins have the access

  Scenario:Validate new fields that are added in Object Manager for Sales order record and Receipt and also validate only System Admins have the access
    Given user navigates to Setup in Sales force Classic
    And user is on the Object manager
    And on the quick find search for Sales order
    When user clicks on Sales Order record
    Then Sales Order External ID appears in the list
    When clicking on the Sales Order External ID and Set field level security
    Then Only System Admin is checked for access
    When user navigates back to sales order record
    Then CRM Invoice Number field appears in the list
    When clicking on the CRM Invoice Number and Set field level security
    Then Only System Admin is checked for access
    When user navigates back to Sales Order Line record
    Then Sales Order Line External ID field appears in the list
    When clicking on the Sales Order Line External ID and Set field level security
    Then Only System Admin is checked for access
    When user navigates back to Objects and Invoice record object
    Then CRM Invoice Number field appears in the list
    When clicking on the CRM Invoice Number and Set field level security
    Then Only System Admin is checked for access
    When user navigates back to Objects and Invoice record object
    Then Invoice External ID field appears in the list
    When clicking on the Invoice External ID and Set field level security
    Then Only System Admin is checked for access
    When user navigates back to Objects and Receipt record object
    Then Payment Type field appears in the list
    When clicking on the Payment Type
    Then Legacy CRM Voucher is included in the Values section for picklist
    