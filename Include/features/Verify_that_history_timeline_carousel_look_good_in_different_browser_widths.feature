Feature: Verify that history timeline carousel look good in different browser widths

Scenario Outline: Verify that history timeline carousel look good in different browser widths and the the lime-green background color and text color is now a darker green (#658e2c) 
    Given I'm on sitecore QA environment
    And I navigate to about page
    And I set the browser to <browserWidth>
    And I inspect the carousel
    Then I see the color is set to #658e2c
      Examples:
        |browserWidth|   
        |1024px      |
        |768px       |
        |360px       |