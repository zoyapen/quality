Feature: when there is no active subscription and candidate has successfully passed the exam should have the ability to pay from member dues page
  

  Scenario: when there is no active subscription 
    Given A member does not have any Active subscription
    And member has successfully passed an exam with Associate intent
    When user navigaes to member dues dashboard http://integration.isc2.org/dashboard/memberdues
    Then user should see Pay With Card and Pay With Voucher and Total amount due should be $50
