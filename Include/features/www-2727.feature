Feature:
This feature allows for assigning and management of the Okta group “CIAM AMF Shopping Cart” from 
Salesforce.  Within Salesforce, in the Contact Details section of the contact, notice that there
is a field called Shopping Cart Access.  By clicking on the checkbox you can toggle the value. 

"""
When the field is checked the user should have the Okta group “AMF Shopping Cart” added to their
groups and when the fields is unchecked, then the Okta AMF Shopping Cart will be removed from 
their Okta groups. As with all modification to the contact in Salesforce, there can be up to 
a 2 minute delay before the record is pulled down from Salesforce and propagated to the down 
streams systems.  
    When the OktaAdapter picks up the contact record from the service bus, it will determine if the Boolean field has been set to true, which will cause the group to be added to the contact, and if the Boolean field is false, then the group will be removed from the user 
in Okta.The management of the Salesforce Shopping Cart access field can be validated by viewing the contact record as pulled down from Salesforce in the CIAM Admin, specifically the value in the
contact record JSON field called "ShoppingCartAccess".  

    Then to validate that the OktaAdapter is correctly acting on the value in “ShoppingCartAccess”, 
locate the user in Okta then click on the Groups tab which shows the list of groups that have 
been assigned to the user.  Note that in Okta, if you are already viewing the user, you will 
need to refresh the page in order for Okta to retrieve the latest values.  
    And as stated above when ShoppingCartAccess is set to true, then CIAM AMF Shopping Cart group will be added to the 
user, and when ShoppingCartAccess is false, the group will be removed.
"""