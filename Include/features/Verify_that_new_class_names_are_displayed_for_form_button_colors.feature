Feature: Verify that new class names are displayed for form button colors 

  Scenario Outline: Check whether the new class names are displayed for form button colors as expected 
    Given I log into sitecore CM
    And I navigate to /sitecore/content/StandardGlobal/Form Button Colors
    When I see the <siteCoreItem>
    Then I see the new <optionFieldValue> for the sitecore item
    Examples:
      |siteCoreItem   |optionFieldValue          |
      |Dark Green     |button-primary            |
      |Light Blue     |button--legacy-light-blue |
      |Lime Green     |button--secondary         |
      |Orange         |button--legacy-dark-orange|
      |Yellow         |button--legacy-dark-yellow|
      