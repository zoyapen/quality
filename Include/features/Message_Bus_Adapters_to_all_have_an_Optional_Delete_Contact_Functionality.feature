Feature: Message Bus Adapters to all have an Optional Delete Contact Functionality
  

  Scenario: Okta CRM adapter to disable or deactivate contacts instead of deleting the contact data
    Given two Contacts records have been "Merge" in Salesforce
    When contact that is the "Victim" will be marked for deletion
     And can be viewed using the CIAM Admin
    Then Okta has the Victim contact's account still exists
     And it will be in a Suspended status

  Scenario: LMS CRM adapter to disable or deactivate contacts instead of deleting the contact data
    Given two Contacts records have been "Merge" in Salesforce
    When contact that is the "Victim" will be marked for deletion
     And can be viewed using the CIAM Admin
    Then Okta has the Victim contact's account still exists
     And the Active Checkbox will be Unchecked
    
    Scenario: CRM adapter to disable or deactivate contacts instead of deleting the contact data
    Given two Contacts records have been "Merge" in Salesforce
    When contact that is the "Victim" will be marked for deletion
     And can be viewed using the CIAM Admin
    Then Okta has the Victim contact's account still exists
     And no Active flag on the contact, just did not delete the contact record.