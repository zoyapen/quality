Feature: First name accepts maximum 40 characters and last name accepts maximum 80 characters while creating an account

  Scenario Outline: Verify that first name accepts maximum 40 characters and last name accepts maximum 80 characters while creating an account
    Given I'm on Sitecore
    And I'm on account creation page
    And I enter first name with <firstNameCharacter> value
    And I enter last name with <lastNameCharacter> value
    Then I see the account creation is successful
    And I check the above email in SalesForce
    Then I see the contact is created successfully
    Examples:
      |firstNameCharacter|lastNameCharacter|
      |40                |80               |

  Scenario Outline: Verify that first name does not accept more than 40 characters and last name more than 80 characters while creating an account
    Given I'm on Sitecore
    And I'm on account creation page
    And I enter first name with <firstNamecharacter> value
    And I enter last name with <lastNameCharacter> value
    Then I see the above fields does not accept the value 
    Examples:
      |firstNameCharacter|lastNameCharacter|
      |41                |81               |
    
    
    
    
    
    
