Feature: CPE Balance value should match Program Term Additional Maintenance Credits Required and Contact Dashboard


  Scenario: Submit CPEs for credit to view values reflected on Sitecore Dashboard
    Given Contact with active Program Tern
    And has "Additional Maintenance Credits Required" still need to be completed
    When Contact submit new CPE credit via CPE portal
    And CPE credit value is Accepted as an Applied CPE
    Then Contact record should be sent to the Bus with the correct "CPEBalance" value
    And credit value reflect correclly for Contact via Sitecore Dashboard 
