Feature: Shipping POC and Primary contact receive email notifications upon fulfillment of material request done via  UPS, Fedex, Wiley, Javelin 

 Scenario Outline:The Shipping POC and Salesperson gets the notification when material request is fulfilled via UPS
    Given A B2C Quote is created with a primary contact that can access email
    And On the Fulfillment editor, Shipping POC is entered that can access email
    And enter all other required information on the fulfillment editor
    When Quote is submitted for approval
    Then the Quote is approved and a fulfllment request is created
    And open the fulfillment request and change the status to ready to fulfill
    And navigate to related list and open the marerial request
    And change the Delivery Type to UPS, UPS Books tracking number to 123456, date books shipped to XX-YY-XXYY date
    And Set status to Fulfilled
    Then Primary and Shipping POC should receive an email with UPS tracking and ship date

        Examples:
        |Product|
        | Book|
        | Digital Book|
        | Kit|

 Scenario Outline:The Shipping POC and Salesperson gets the notification when material request is fulfilled via Fedex
    Given A B2C Quote is created with a primary contact that can access email
    And On the Fulfillment editor, Shipping POC is entered that can access email
    And enter all other required information on the fulfillment editor
    When Quote is submitted for approval
    Then the Quote is approved and a fulfllment request is created
    And open the fulfillment request and change the status to ready to fulfill
    And navigate to related list and open the marerial request
    And change the Delivery Type to Fedex, Fedex Books tracking number to 123456, date books shipped to XX-YY-XXYY date
    And Set status to Fulfilled
    Then Primary and Shipping POC should receive an email with Fedex tracking and ship date

        Examples:
        |Product|
        | Book|
        | Digital Book|
        | Kit|
 
 
 Scenario Outline:The Shipping POC and Salesperson gets the notification when material request is fulfilled via eBook Wiley
    Given A B2C Quote is created with a primary contact that can access email
    And On the Fulfillment editor, Shipping POC is entered that can access email
    And enter all other required information on the fulfillment editor
    When Quote is submitted for approval
    Then the Quote is approved and a fulfllment request is created
    And open the fulfillment request and change the status to ready to fulfill
    And navigate to related list and open the marerial request
    And change the Delivery Type to Ebook Wiley, eBook access code to 123456, date books shipped to XX-YY-XXYY date
    And Set status to Fulfilled
    Then Primary and Shipping POC should receive an email with Wiley book and Digital Access code

        Examples:
        |Product|
        | Book|
        | Digital Book|
        | Kit|
 
 
 Scenario Outline:The Shipping POC and Salesperson gets the notification when material request is fulfilled via eBook Javelin
    Given A B2C Quote is created with a primary contact that can access email
    And On the Fulfillment editor, Shipping POC is entered that can access email
    And enter all other required information on the fulfillment editor
    When Quote is submitted for approval
    Then the Quote is approved and a fulfllment request is created
    And open the fulfillment request and change the status to ready to fulfill
    And navigate to related list and open the marerial request
    And change the Delivery Type to eBook Javelin, eBook access code to 123456, date books shipped to XX-YY-XXYY date
    And click on the product and make a note of the Javelin Ebook link
    And Set status to Fulfilled
    Then Primary and Shipping POC should receive an email with Javelin book that has Digital Access code and Ebook link

        Examples:
        |Product|
        | Book|
        | Digital Book|
        | Kit|