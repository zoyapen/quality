Feature: Member submits PDI CPE
  As a ISC2 member, I should be able to submit CPEs earned for completing PDI course

  Scenario: Professional submits PDI CPE in between active term
    Given Professional has active maintenance term
    When submits "<CPEType>" CPE
    |CPEType|
    | Group A|
    | Group B|
    Then CPE is accepted
    And respective CPE count gets incremented
    
  Scenario: Associate submits PDI CPE between active term
    Given Associate has active maintenance term
    When submits Group A CPE
    Then CPE is accepted
    And CPE count gets incremented 
    
  Scenario Outline: PDI CPE selected for Audit
    Given <MemberType> has active maintenance term
    When submits <CPEType>
    Then CPE has status "Selected for Audit"
    And Audit case gets created
    
    Examples:
      |MemberType|CPEType|
      | Professional|Group A|
      | Professional|Group B|
      | Associate| Group A|
    
  Scenario: Professional submits PDI CPE in grace period
    Given Professional has active maintenance term
    And current date is within 90 days of term end date
    When submits "<CPEType>" CPE
    |CPEType|
    | Group A|
    | Group B|
    Then CPE is accepted
    
  Scenario: Associate submits PDI CPE in grace period
   Given Associate has active maintenance term
   And current date is within 90 days of term end date
   When submits Group A CPE dated before term end date
   Then CPE is accepted
   And CPE count gets incremented
   
  Scenario Outline: CPE with activity date outside of term end date
    Given <MemberType> has active maintenance term
    And current date is within 90 days of term end date
    When submits <CPEType> dated after term end date
    Then CPE is not accepted
    
    Examples:
      |MemberType|CPEType|
      | Professional|Group A|
      | Professional|Group B|
      | Associate| Group A|
