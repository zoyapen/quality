Feature: Intructor fee currency field supports cents
  As an instructor specialist, I should be able to see and add value in Instructor Fee field in cents.  

  Scenario: Instructor specialist able to enter dollar amount in cents
    Given member is an instructor of ISC2
    When instructor specialist creates Instructor request
    Then Instructor fee field accepts amount in cents
    
  Scenario: Instructor specialist able to view dollar amount in cents
    Given member is an instructor of ISC2
    When instructor specialist reviews Instructor request
    Then Instructor fee field has value visible in cents
