Feature: Verify that Payload output from pearson part of async papi conforms to transformation

Scenario Outline: Verify that first name with different boundary value length is accepted and POST is successful
    Given I'm on workbench
    And I set the <firstName> to <characterLength> in request payload 
    And I set the <contactNumber> and <email>
    When I POST a request
    Then I see the POST was successful
    And I do not see any errors
    Examples:
      |firstName                                |characterLength|contactNumber|email                      |  
      |LetstestsomethingawesomeYesAw            |    29         |123456       |t.e.stingtime@gmail.com    |
      |LetstestsomethingawesomeYesAwe           |    30         |145623       |t.e.s.tingtime@gmail.com   |
      |LetstestsomethingawesomeYesAwes          |    31         |124562       |t.e.s.t.ingtime@gmail.com  |
      |LetstestsomethingawesomeYesAwesomeketste |    40         |124561       |t.e.s.t.i.ngtime@gmail.com |

Scenario Outline: Verify that middle name with different boundary value length is accepted and POST is successful
    Given I'm on workbench
    And I set the <middleName> to <characterLength> in request payload 
    And I set the <contactNumber> and <email>
    When I POST a request
    Then I see the POST was successful
    And I do not see any errors
    Examples:
      |middleName                                |characterLength|contactNumber|email                         |  
      |areyoutiredtestingareyoutired            |    29          |987654       |t.e.s.t.in.g.t.i.me@gmail.com |
      |areyoutiredtestingareyoutireda           |    30          |987456       |t.e.s.t.in.g.t.i.m.e@gmail.com|
      |areyoutiredtestingareyoutiredar          |    31          |987123       |t.e.s.ti.n.gtime@gmail.com    |
      |areyoutiredtestingareyoutiredareyoutired |    40          |987000       |t.e.s.ti.n.gtime@gmail.com    |

Scenario Outline: Verify that last name with different boundary value length is accepted and POST is successful
    Given I'm on workbench
    And I set the <lastName> to <characterLength> in request payload 
    And I set the <contactNumber> and <email>
    When I POST a request
    Then I see the POST was successful
    And I do not see any errors
    Examples:
      |lastName                                                    |characterLength|contactNumber|email                          |  
      |ilovetestingwebsiteilovetestingwebsiteilovetestin           |    49          |786000       |t.e.s.t.in.g.t.i.me@gmail.com |
      |ilovetestingwebsiteilovetestingwebsiteilovetesting          |    50          |786012       |t.e.s.t.in.g.t.i.m.e@gmail.com|
      |ilovetestingwebsiteilovetestingwebsiteilovetestingi         |    51          |786123       |t.e.s.ti.n.gtime@gmail.com    |
      |ilovetestingwebsiteilovetestingwebsiteilovetestingilovetesti|    60          |786987       |t.e.s.ti.n.g.ti.m.e@gmail.com | 

Scenario Outline: Verify that city with different boundary value length is accepted and POST is successful
    Given I'm on workbench
    And I set the <city> to <characterLength> in request payload 
    And I set the <contactNumber> and <email>
    When I POST a request
    Then I see the POST was successful
    And I do not see any errors
    Examples:
      |city                                                        |characterLength|contactNumber |email                            |  
      |newyrokcityisgreatnewyorkcityis                             |    31          |765123       |t.e.st.i.n.gtime@gmail.com       |
      |newyrokcityisgreatnewyorkcityisg                            |    32          |765143       |t.e.st.i.n.g.time@gmail.com      |
      |newyrokcityisgreatnewyorkcityisga                           |    33          |765000       |t.e.st.i.n.g.t.ime@gmail.com     |     
      |newyrokcityisgreatnewyorkcityisgreatnewy                    |    40          |765111       |t.e.st.i.n.g.t.i.m.e@gmail.com   | 

Scenario Outline: Verify that chinese/japanese/korean characters and diacritical marks is accepted and POST is successful
    Given I'm on workbench
    And I set the <firstName> in request payload 
    And I set the <contactNumber> and <email>
    When I POST a request
    Then I see the POST was successful
    And I do not see any errors
    Examples:
      |firstName                                                   |contactNumber |email                           |  
      |杰伊哦娜哦伊娜哦娜勒开伊哦吉艾艾弗诶吾伊娜勒开伊儿伊娜诶吾  |765123        |t.e.st.in.gti.me@gmail.com      |
      |くいりりらてりいかといみまらんりにはい                      |765143        |t.e.st.i.n.g.time@gmail.com     |
      |논의하다                                                    |765000        |t.e.st.in.gt.im.e@gmail.com     |     
      |learnèdcoöperateôperàtorpādā                                |444999        |t.e.st.in.gt.im.e@gmail.com     |
