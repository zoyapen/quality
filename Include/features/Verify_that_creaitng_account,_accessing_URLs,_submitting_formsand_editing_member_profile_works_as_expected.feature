Feature: Verify that creating account, accessing URLs, submitting forms and editing member profile works as expected
  
  Scenario: Verify that creating an account is successful
    Given I'm on home page
    When I create a new account
    And I enter all the required details
    Then I'm able to sign into the new account
    And I can access the member profile

  Scenario Outline: Verify that navigation to URLs is successful and shows the correct page title
    Given I log into QA env
    When I navigate to <pageName>
    Then I see the correct <pageTitle>
    And the buttons are left aligned
    Examples:
        |pageName                             |pageTitle                                                                  |
        |certifications-overview              |Information Security Certifications                                        |
        |register-for-exam                    |Register for Exam                                                          |
        |endorsement                          |Get Endorsement                                                            |
        |benefits-of-membership               |Verify Certification or Designation                                        |
        |digital-badges                       |Digital Badges from Acclaim                                                |
        |certifications-cissp                 |CISSP – The World's Premier Cybersecurity Certification                    |
        |certifications-sscp                  |SSCP – The Premier Security Administrator Certification                    |
        |certifications-ccsp                  |CCSP – The Industry’s Premier Cloud Security Certification                 |
        |certifications-cap                   |CAP – Security Assessment and Authorization Certification                  |
        |certifications-csslp                 |CSSLP – The Industry’s Premier Secure Software Development Certification   |
        |certifications-hcispp                |HCISPP – The HealthCare Security Certification                             |
        |certifications-cissp-concentrations  |CISSP Concentrations                                                       |
        |member-dashboard                     |Certification Status                                                       |
        |member-dashboard-profile             |Employment Information                                                     |
        |member-home                          |home page content                                                          |
        |preferences                          |Communication Preferences and Multi-Factor Authentication                  |
        |benefits                             |Benefits of (ISC)² Membership                                              |
        |CPEs                                 |Add New CPE                                                                |         
        |maintenance                          |Checkout                                                                   |
    
 Scenario: Verify that editing the phone field in the member profile personal information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the phone field value  
   And I click the save button
   Then I see the updated phone number on member profile page

 Scenario: Verify that editing the mailing address field in the member profile personal information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the mailing address field value  
   And I click the save button
   Then I see the updated mailing address on member profile page
   
 Scenario: Verify that searching a record on member verification form returns appropriate result
   Given I'm on member verification form
   And I search for an existing member
   Then I see correct details of the above member
 
 Scenario: Verify that downloading a CISSP Guide and entering details shows correct certification of interest in the backend
   Given I'm on CISSP Certifications page
   And I click the download button
   And I enter the personal information
   And I click the submit button
   When I check the leads in the backend system for the above contact
   Then I see the correct lead with certification of interest
  
    