Feature: Reinstating suspended contact
  As an ISC2 customer, I should be able to pay AMF and reinstatement fees online. 

  Scenario Outline: Member reinstating with reinstatement fees
    Given contact has <SubscriptionType> in suspended status
    When member navigate to dashboard
    And clicks on reinstatement button
    Then new sales order is created with relavent sales order lines in Salesforce  
    And navigated to shopping cart with visible <BackDues> and reinstatement fee
    
  Examples:
    |SubscriptionType| BackDues|
    | Professional | $125|
    | Associate | $50|

  Scenario Outline: Member reinstating without reinstatement fees
    Given contact has <SubscriptionType> in suspended status
    When member navigate to dashboard
    And clicks on reinstatement button
    Then new sales order is created with relavent sales order lines in Salesforce  
    And navigated to shopping cart with visible <BackDues>
    
  Examples:
    |SubscriptionType| BackDues|
    | Professional | $125|
    | Associate | $50|
    
