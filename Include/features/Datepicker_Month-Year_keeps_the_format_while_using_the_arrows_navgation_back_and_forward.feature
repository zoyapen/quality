Feature: Datepicker Month-Year keeps the format while using the arrows navgation back and forward
 

  Scenario: Datepicker keeps correct calendar formate while using back and forward arrows  
    Given I have nagigated to either "/News-and-Events/Event-Calendar" or "/Training"
    When I use "Datepicker" to select dates for filtering
     And click the back arrow i.e. past date
    Then Datepicker does not allow me to pick a past date
     And calendar formate is not effected
    When I use "Datepicker" to select furture dates for filtering
    Then "Datepicker" displays the correct formate
     And filters based on any non-past date

    
