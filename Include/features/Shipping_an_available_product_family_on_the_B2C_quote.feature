Feature: Shipping an available product family on the B2C quote
  
  Scenario: Ship a B2C Product using IAP kit goes to Approved when submitted
    Given a B2C Opportunity has been created
     And payment method has been set
    When IAP kit has been added 
     And Shipping line Saved
     And Fulfillment info completed
    Then the Submitted quote goes to "Approved" status
  
