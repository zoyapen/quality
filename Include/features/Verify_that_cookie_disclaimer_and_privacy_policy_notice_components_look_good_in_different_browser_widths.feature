Feature: Verify that cookie disclaimer and privacy policy notice components look good in different browser widths

Scenario Outline: Verify that cookie disclaimer notice component look good in different browser widths
    Given I'm on sitecore QA environment
    And I navigate to home page
    And I set the browser to <browserWidth>
    Then I see the cookie disclaimer component looks good
      Examples:
        |browserWidth|   
        |1024px      |
        |768px       |
        |360px       |

Scenario Outline: Verify that privacy policy notice component look good in different browser widths
    Given I'm on sitecore QA environment
    And I navigate to sign in page
    And I set the browser to <browserWidth>
    And I sign in with valid credentials
    Then I see the privacy policy notice component looks good on Dashboard
      Examples:
        |browserWidth|   
        |1024px      |
        |768px       |
        |360px       |
    
    