Feature: Decommission Certification Replication
  As a member services staff, when relevant goals on candidacy term or maintenance term are marked complete, no certification event record is created.

  Scenario Outline: Contact getting new maintenance term
    Given contact has passed exam for <Certificate Type>
    When all goals are marked complete on candidacy term
    Then no certification event is created
    
  Examples:
    |Certificate Type|
    | Professional | 
    | Associate | 
    | Concentration | 
  
  Scenario Outline: Contact renewing certificate
    Given member has active maintenance term for <Certificate Type>
    When all goals are marked complete on maintenance term
    Then no certification event is created
    
  Examples:
    |Certificate Type|
    | Professional | 
    | Associate | 
    | Concentration |
    
