Feature: For any IAP type accounts, there is no Instructor Request, no Class roster, and no Venue Reques 
  Scenario: For any IAP type accounts, there is no Instructor Request, no Class roster, and no Venue Reques is created on the instructor request and also material requests that gets created based on number of products should have ready to fill status
    Given An IAP type account is selected
    And create a B2B Quote and navigate to adding products
    And add a Kit and an IAP product
    And Navigate to fulfillment information and fill all the necessary information and ship by date more than 60 days
    And move the Quote to approved
    And Navigate to the Fulfillment request that is created on the opportunity
    When the status on the fulfillment request is changed to Ready to fulfill
    Then The materail requests gets created for the products on the related list
    When navigate to each material request
    Then the staus should be ready to fulfill and there should not be any Instructor Request, no Class roster, and no Venue Request
    
