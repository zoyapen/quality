Feature: Verify that text alignment looks good and the height is set to auto for submit buttons

Scenario Outline: Verify that text alignment looks good and the height is set to auto for submit buttons
    Given I'm on sitecore QA environment
    And I navigate to <page>
    Then I see the text alignment is correct
    And I inspect the element
    Then I see the height is set to auto
      Examples:
        |page                                                                                                                                                               |   
        |https://wwwqa.isc2.org/Member-Discount-Program                                                                                                                     |                                                                       
        |https://wwwqa.isc2.org/Certifications/Ultimate-Guides/CISSP?utm_campaign=H-HQ-CISSPultimateguide&utm_source=isc2web&utm_medium=button&utm_content=cissppagetop     |
        |https://wwwqa.isc2.org/training/private-on-site                                                                                                                    |
    