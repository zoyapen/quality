Feature: Fonteva badge issued and deactivated as per different statuses of the Program Profile
  <Some interesting description here>

  Scenario: Maintenance term is created for the first time
    Given relevant goals are marked complete for candidacy term
    And certification event is generated
    When first maintenance term is issued by CRM 
    Then fonteva badge for the program is issued after the successful overnight batch job
    
  Scenario: Member has been banned
    Given member has active maintenance term
    And has violated ISC2 policy
    When member services ban the member
    Then Program profile is set to banned status 
    And fonteva badge is deactivated immediately
    
  Scenario: Program Profile in Terminated status
    Given Member has different active certifications 
    And has a maintenance term issued
    When PP is changed to Terminated status
    Then fonteva badge for that profile is deactivated immediately
  
  Scenario: Candidate has just passed an exam
    Given Member has different certifications 
    When new exam result is created in SF 
    And has a candidacy term issued for it
    Then No badge is issued to profile for this exam after the successful overnight batch job
    
  Scenario: Member has been terminated and again reactivated
    Given member has not fulfilled the CPE goal or paid the annual membership fee
    When system automatically terminate the member ( integration that sets from CRM -> SF)
    Then Program profile is set to terminated status
    And the fonteva badge is deactivated 
    When member services reinstates the member 
    Then same Program Profile status is changed to maintenance
    And member gets a new active fonteva badge
