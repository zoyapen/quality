Feature: As Finance user, once the Opportunity stage is Payment received and the Quote has a voucher product then the users can access Asset & Voucher from Opportunity related list

  Scenario: Finance users should be able to access vouchers and assets from Opportunity related list
   Given Quote has a voucher product and is approved 
   And Quote opportunity stage is Agreement signed
   When The payment is received and Opportunity stage is Payment received
   Then Finance user can access the assets and vouchers from the opportunity related list
