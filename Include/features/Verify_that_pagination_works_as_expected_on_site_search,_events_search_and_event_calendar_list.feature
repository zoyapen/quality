Feature: Verify that pagination works as expected on site search, events search and event calendar list

  Scenario Outline: Verify that pagination works as expected on site search, events search and event calendar list
    Given I'm on Sitecore
    And I set the browser <width>
    And I navigate to <searchComponent>
    And I enter "Certifications"
    And I click the search button
    Then I see the results
    And I scroll to the bottom of the page
    Then I see the pagination
    And I navigate to the other pages 
    Then I see the navigation is successful
        Examples:
          |width  |searchComponent|
          |375px  |site search    |
          |768px  |site search    |
          |1024px |site search    |
          |375px  |events search  |
          |768px  |events search  |
          |1024px |events search  |
          |375px  |event calendar list |
          |768px  |event calendar list |
          |1024px |event calendar list |

