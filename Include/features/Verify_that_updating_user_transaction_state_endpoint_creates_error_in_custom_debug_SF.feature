Feature: Verify that updating user transaction state endpoint creates error in custom debug SF
  
  Scenario: Verify that updating user transaction state endpoint and set IDP status to error creates error in custom debugs SF
    Given I launch rest explorer 
    And I set the ConnectedSystemStatusId to a unique id
    And I set the error message in the request body 
    And I set the IDP status to "ERROR"
    And I send a POST to /services/apexrest/mulesoft/user-transaction-state/
    Then I see a valid response for the above POST
    And I navigate to SF for the above connected system status id
    Then I see "ERROR" for Okta status in the detailed section
    And I navigate to related section for the above connected system status id
    Then I see error logs under custom debugs
    And I see the cause as "failed at Okta/Mongo"
    And I see the above error message that was sent in the POST request body
    And I click the custom debug name
    Then I see all the correct details 

  Scenario: Verify that updating user transaction state endpoint and set IDP status to created does not have errors in custom debugs SF
    Given I launch rest explorer 
    And I set the ConnectedSystemStatusId to a unique id
    And I set the error message in the request body to ""
    And I set the IDP status to "CREATED"
    And I send a POST to /services/apexrest/mulesoft/user-transaction-state/
    Then I see a valid response for the above POST
    And I navigate to SF for the above connected system status id
    Then I see "CREATED" for Okta status in the detailed section
    And I navigate to related section for the above connected system status id
    Then I do not see error logs under custom debugs for the above POST

  Scenario: Verify that updating user transaction state endpoint and set IDP status to created and LMS status to error does not have errors in custom debugs SF
    Given I launch rest explorer 
    And I set the ConnectedSystemStatusId to a unique id
    And I set the error message in the request body to ""
    And I set the IDP status to "CREATED"
    And I set the LMS status to "ERROR"
    And I send a POST to /services/apexrest/mulesoft/user-transaction-state/
    Then I see a valid response for the above POST
    And I navigate to SF for the above connected system status id
    Then I see "CREATED" for Okta status in the detailed section
    And I see "ERROR" for D2L status in the detailed section
    And I navigate to related section for the above connected system status id
    Then I do not see error logs under custom debugs for the above POST