Feature: Member - Already has current subscription and RequestStatus as Membership Complete and Itemname Professional Membership - Annual Fee

  Scenario:
      Scenario: Member - Already has current subscription and RequestStatus as Membership Complete and Itemname Professional Membership - Ability to link to Pay AMF from the Dashboard when SubscriptionPaidThroughDate is prior 45 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is 45 days prior to todays date
    And Endorsement request status is Endorsement complete
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then Member should not see any link to pay dues  
    
  Scenario: Member - Already has current subscription and RequestStatus as Membership Complete and Itemname Professional Membership - Ability to link to Pay AMF from the Dashboard when SubscriptionPaidThroughDate is within 90 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is more than 90 days from today's date
    And Endorsement request status is Endorsement complete
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then Member should not see any link to pay dues