Feature: Verify that unused HTML and CSS files are removed

  Scenario Outline: Check whether unused HTML files are removed
    Given I navigate to <htmlPage>
    Then I do not see any contnet in the above html page
    Examples:
        |htmlPage                       |
        |history-and-details.html       |
        |member-cpe-wizard-add-new.html |
        |member-cpes.html               |
        |payment-summary.html           |
        |mfa_settings_vue.html          |
 

  Scenario Outline: Verify that the html pages for secondary nav margin decreases at browser’s max-width of 767px 
    Given I navigate to <htmlPage>
    When I set the browser's max width to 767px
    Then I see the secondary nav margin decreases
    Examples:
        |htmlPage                                  |
        |member-dashboard-candidate-component.html |
        |member-dashboard-dues-tile.html           |
        |member-dashboard.html                     |
        |member-dues-paid.html                     |
        |member-dashboard.html                     |
        |member-dues.html                          |
        |member-preferences.html                   |
        |member-profile-edit.html                  |
        |member-profile.html                       |

@Regression
 Scenario: Verify that edit option is displayed for personal and employment information on member profile page
   Given I login as a user
   When I navigate to member profile page
   Then I see an edit option for personal and employment information

 Scenario Outline: Verify that correct fields are displayed in the edit section of personal information
   Given I'm on member profile page
   When I edit the personal information
   Then I see <fields> in the personal information
   Examples:
     |fields          | 
     |email           |
     |secondary email |
     |first name      |
     |middle name     |
     |last name       |
     |nick name       |
     |phone           |
     |country/region  |
     |street          |
     |city            |
     |state/province  |
     |postal code     |
 
 Scenario: Verify that editing the phone field in the member profile personal information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the phone field value  
   And I click the save button
   Then I see the updated phone number on member profile page

 Scenario: Verify that editing the mailing address field in the member profile personal information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the mailing address field value  
   And I click the save button
   Then I see the updated mailing address on member profile page

 Scenario: Verify that editing the billing address field in the member profile personal information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the billing address field value  
   And I click the save button
   Then I see the updated billing address on member profile page

 Scenario: Verify that editing the billing address field in the member profile information is successful
   Given I'm on member profile page
   When I edit the personal information
   And I edit the billing address field value  
   And I click the save button
   Then I see the updated billing address on member profile page

 Scenario Outline: Verify that correct fields are displayed in the edit section of employment information
   Given I'm on member profile page
   When I edit the employment information
   Then I see <fields> in the employment information
   Examples:
     |fields          | 
     |employer name   |
     |title           |
     |work phone      |

 Scenario: Verify that editing the title field in the member profile employment information is successful
   Given I'm on member profile page
   When I edit the employment information
   And I edit the title field value  
   And I click the save button
   Then I see the updated title on member profile page

 Scenario Outline: Verify that the close button works as expected
   Given I'm on member profile page
   When I edit the <information> section
   And I click the close button
   Then I do not see an option to edit the fields
   Examples:
     |information           |
     |personal information  |
     |employment information|
           