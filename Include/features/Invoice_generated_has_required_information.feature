Feature: Invoice generated has required information
  As a customer support user, I should see required information on invoice. 

  Scenario: Member is in grace period
    Given Member has active subscription
    And is in grace period
    And invoice is created
    When member services person views invoice
    Then required information is visible 
