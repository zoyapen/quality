Feature: Field on Associate Program Profile page layout counting total number of maintenance terms issued 

@AMS-1985

  Scenario: Person is Associate of ISC2 since last 3 years  
    Given person is an Associate of ISC2
    And has 3 maintenance terms
    And current maintenance term end date is in this month
    And 'Total No of Program Terms' field on Program Profile page has value 3
    When associate completes the required goals
    Then new renewal maintenance term is issued
    When batch job is executed manually
    Then 'Total No of Program Terms' field is incremented by 1
    
  Scenario: Person just becomes an Associate of ISC2  
    Given person becomes an Associate of ISC2
    And has a candidacy term issued
    And 'Total No of Program Terms' field on Program Profile is null
    When associate is issued a new maintenace term
    And batch job is executed manually
    Then value of 'Total No of Program Terms' field is incremented by 1
    
