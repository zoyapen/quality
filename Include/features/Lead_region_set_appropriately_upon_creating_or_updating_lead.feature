Feature: Lead region set appropriately upon creating or updating lead 

  Scenario Outline: Creating lead
    Given person fills out online form
    When new form is submitted with country listed in <Region>
    Then new lead is created
    And region is set to <Region> appropriately
    
    Examples:
      |Region|
      | APAC|
      | EMEA|
      | LATAM|
      | NAR|
    
  Scenario Outline: Updating lead
    Given lead is present in Salesforce
    When country is updated on lead 
    And changed to one listed in <Region>
    Then region is updated to <Region>
    
    Examples:
      |Region|
      | APAC|
      | EMEA|
      | LATAM|
      | NAR|
