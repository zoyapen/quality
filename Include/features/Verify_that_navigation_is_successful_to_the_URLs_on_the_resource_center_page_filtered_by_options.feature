Feature: Verify that navigation is successful to the URLs on the resource center page filtered by options 

  Scenario Outline: Check whether the navigation is successful to the URLs on the resource center page filtered by options
    Given I navigate to <resourceCenterFilter>
    Then I see correct content for the above resource center filter
    Examples:
        |resourceCenterFilter |
        |Study Aids           |
        |Guides               |
        |White Papers         |
        |Research             |
        |eBooks               |
        |Data Sheets          |
        |Video                |
        |Infographics         |

  Scenario Outline: Check whether the navigation is successful to the mini-navs on the resource center page
    Given I navigate to resource-center page
    And I click the <resourceCenterFilter> mini-navs
    Then I see correct content for the above resource center filter mini-navs
    Examples:
        |resourceCenterFilter |
        |Study Aids           |
        |Guides               |
        |White Papers         |
        |Research             |
        |eBooks               |
        |Data Sheets          |
        |Video                |
        |Infographics         |