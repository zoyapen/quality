Feature: Person unable to access digital certificate before getting certified
 As a customer when I try to access the digital certificate after passing my exam, I should see a white screen with message till I get certified.

  Scenario Outline: Person just passed the exam 
    Given person passed exam for <Certificate>
    And has a program profile issued with Candidate status
    And has an activecandidacy term issued
    When person log in to member Dashboard
    And click on Digital certificate button
    Then pdf opens up with white screen and a message explaining why digital certificate didn't generate

  Examples:
  |Certificate|
  | CISSP     |
  | CAP       |
  | CCSP      |
  | SSCP      |
  | CSSLP     |
  | HCISPP    |
  | ISSAP     |
  | ISSMP     |
  | ISSEP     |