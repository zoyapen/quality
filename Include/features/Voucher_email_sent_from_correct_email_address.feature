Feature: Voucher email sent from correct email address
As a sales representative, I should see voucher notification email sent from correct email address

  Scenario: Exam Voucher sent
    Given exam voucher quote is created 
    When exam vouchers are created
    Then voucher notification email goes out from correct email address
    And sales person is copied on that email
    
  Scenario: AMF Voucher sent
    Given AMF voucher quote is created 
    When AMF vouchers are created
    Then voucher notification email goes out from correct email address
    And sales person is copied on that email
    
  Scenario: Training Voucher sent
    Given training voucher quote is created 
    When training vouchers are created
    Then voucher notification email goes out from correct email address
    And sales person is copied on that email
