Feature: Member suspended when failed to pay the AMF
As a member services staff, I should be able to see the subscription suspended when member fail to pay the AMF on time and go out of grace period

@AMS-2041

  Scenario: Member in grace period with unpaid AMF
    Given Member has an active maintenance term
    And has active subscription
    When current date has passed subscription end date
    And 'Days Lapsed' field has value between -1 and -90 
    And member has not paid AMF
    Then subscription will be in 'Active' status
    And program profile status will be 'Maintenance'
    
  Scenario: Member out of grace period with unpaid AMF
    Given Member has an active maintenance term
    And has active subscription
    When current date has passed subscription end date
    And 'Days Lapsed' field value is -91
    And member has not paid AMF
    Then subscription will be in 'Expired' status
    And program profile status will be 'Suspended'
    
