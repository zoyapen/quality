Feature: MS: update password validation in Okta-sapi

  Scenario Outline: Verify that Okta-sapi password field at least accepts 8 characters with different combinations of characters
    Given I execute Okta-sapi
    And I set the <password> in the payload
    And I execute the request
    Then I see status response with 201 
    And I see the status set to "ACTIVE"
        Examples:
            |password                           |
            |Ca!#$%&'()*+com                    |
            |Ca,-./:<=>?@[]com                  |
            |Ca05-04-2020-01-41-02@mailinator.com|
    
    
  Scenario: Verify that Okta-sapi password field accepts 72 characters
    Given I execute Okta-sapi
    And I set the <password> with 72 characters in the payload
    And I execute the request
    Then I see status response with 201 
    And I see the status set to "ACTIVE"
    
Scenario: Verify that Okta-sapi password field does not accpet >= 73 characters
    Given I execute Okta-sapi
    And I set the <password> with 73 characters in the payload
    And I execute the request
    Then I see 400 status code
    And the message stating bad request
    
Scenario: Verify that Okta-sapi password field does not accept <= 7 characters
    Given I execute Okta-sapi
    And I set the <password> with 7 characters in the payload
    And I execute the request
    Then I see 400 status code
    And the message stating bad request
    
Scenario: Verify that Okta-sapi password field accepts at least 8 characters, 1 Upper, 1 lower, and 1 special character (of the approved special char)
    Given I execute Okta-sapi
    And I set the <password> with at least 8 characters, 1 Upper, 1 lower, and 1 special character
    And I execute the request
    Then I see 201 status code
    And I see the status set to "ACTIVE"
    