Feature: Member with cancelled OE in past able to pay AMF through Dashboard
  As a member, I should be able to pay AMF once my current OE is approved and I have past ER cancelled.

  Scenario: Candidate with past cancelled OE
    Given person has existing cancelled OE
    When navigates to OE app
    And submits new OE for same program 
    Then ER is received in Salesforce
    When Member services approves ER
    Then Endorsement goal on candidacy term is marked complete
    When member navigate to Dashboard
    Then AMF due visible
    And option to pay visible to member
    
  Scenario: Candidate with past declined OE
    Given person has existing ER in Endorsement Declined status
    When navigates to OE app
    Then person doesn't get option to submit OE for same program
   
