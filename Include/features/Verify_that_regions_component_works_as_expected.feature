Feature: Verify that regions component works as expected

  Scenario Outline: Verify that regions component works as expected
    Given I'm on Sitecore
    And I set the browser <width>
    And I navigate to regions component
    And I open <componentName>
    Then I see the contact information and other details
      Examples:
            |width  |component   |
            |375px  |Americas    |
            |375px  |EMEA        |
            |375px  |ASIA-PACIFIC|
            |768px  |Americas    | 
            |768px  |EMEA        |
            |768px  |ASIA-PACIFIC|
            |1024px |Americas    | 
            |1024px |EMEA        |
            |1024px |ASIA-PACIFIC|
