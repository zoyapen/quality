Feature: Subscription paid through date set once AMF is paid using voucher
  As a Member Services Staff, after an AMF payment is made using voucher through the Fonteva shopping cart (or back office), I need the Subscription that gets created/extended to have a subscription paid through date matching the End Date of the subscription

  Scenario Outline: Customer pays AMF for getting membership
    Given customer has active candidacy term for <Membership Type> membership
    And has only membership goal incomplete
    When pay <Outstanding AMF> using voucher
    Then new subscription is created
    And subscription paid through date matches subscription end date
    
    Examples:
     | Membership Type | Outstanding AMF |
     | Professional | $125|
     | Associate | $50 |

  Scenario Outline: Customer pays AMF for renewals before current subscription end date
    Given member has active <Membership Type> subscription
    And current date is within 45 days of subscription end date
    When pay <Outstanding AMF> using voucher
    Then subscription end date is extended for next one year
    And subscription paid through date matches subscription end date
    
    Examples:
     | Membership Type | Outstanding AMF |
     | Professional | $125|
     | Associate | $50 |
     
  Scenario Outline: Customer pays AMF for renewals once in grace period
    Given member is in grace period for <Membership Type> membership
    And current date is within 90 days of passing subscription end date
    And subscription paid through date matches subscription end date
    When invoice is created
    Then subscription end date is extended for next one year
    But subscription paid through date field is not updated 
    When member pay <Invoiced AMF> using voucher
    Then subscription end date remains the same
    And subscription paid through date matches subscription end date
 
  Examples:
     | Membership Type | Invoiced AMF |
     | Professional | $125|
     | Associate | $50 |
