Feature: Contacts created or updated in Salesforce are reflected in CRM when the toggle is set to listen to Salesforce

"""The purpose of the CRM Adapter is to read new or updated contact records from the service bus and 
write the data to CRM.  Upon reading the data from the service bus, the most basic validation is 
performed to ensure that a ContactId and Email address are present.  After that the adapter 
authenticates against CRM, supplying the required data attributes of UserName and Password.  
If authentication fails, the adapter writes a log message to Microsoft.ApplicationInsights.  
If authentication was successful, the LMS Adapter will attempt to make a REST API call to LMS to 
lookup the contact record by ContactId, if the record does not exist, a new record will be 
inserted, otherwise the existing record will be updated.  If there are any issues with writing 
the contact record to LMS, the error will be logged in Microsoft.ApplicationInsights.   The 
Service Bus and its adapters are not responsible for any validation of Contact Records besides 
the presence of the ContactId and Email address.  It is the responsibility of the Sitecore WWW 
and Salesforce to ensure field lengths, character sets, text formats validation are performed 
as needed."""

Scenario: CRM Adapter - (Happy Path) Modified Contact Records are read from the service bus and “inserts” are applied to CRM
    Given the message in the Service Bus topic for CRM, the message is read from the service bus that contains 1 or more contact records that are to be applied to CRM 
    When a REST API call is made to CRM to determine if the contact record can be found by “ContactId”
     And it does not exist 
    Then a new contact record is created in CRM with the properties that have been supplied
     And can be validate by logging into CRM and clicking Contacts in the left Nav and searching for the contact entered in the WWW Site.


Scenario: CRM Adapter - (Happy Path) Modified Contact Records are read from the service bus and “updates” are applied to CRM
    Given the message in the Service Bus topic for CRM, the message is read from the service bus that contains 1 or more contact records that are to be applied to CRM 
    When a REST API call is made to CRM to determine if the contact record can be found by “ContactId”
     And it does exist 
    Then the existing contact record will be updated with the properties that have been supplied
     And can be validate by logging into CRM and clicking Contacts in the left Nav and searching for the contact entered in the WWW Site.
