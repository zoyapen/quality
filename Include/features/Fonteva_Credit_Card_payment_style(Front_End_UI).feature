Feature:  Fonteva Credit Card payment style(Front End UI)
As a member when I go to go the AMF shopping cart check out page, I should see the specific format of UI.

@AMS-2037

Scenario Outline: Customer logins to the Member Payment Gateway page
 Given a contact has a sales order 
 And a Sales line Order with Item <MemberFees>
 And sales order id 
 When navigated to Test shopping cart URL 
 And change the sales order id based on current sales order 
 Then the shopping cart displayed is for current sales order 
 And the amount displayed should be <Amount>
 And no account Icon on the top right, No credit card images, No Credit card and e check tab, Exp month and year to full words, Nothing except the order total no pieces related to order payment
 When we click on the home page
 Then should return to the home dashboard page
 
 Examples:
   |MemberFees| Amount|
   |Professional Membership - Annual Fee | $125|
   |Associate Membership - Annual Fee | $50|