Feature: Voucher error handling in member Dashboard
  As a customer, I should see error message when I use incorrect voucher for purchasing new or renewing membership 

  Scenario: Error type of Incorrect voucher type
    Given person has passed exam
    And member services has approved OE
    When person navigate to member dashboard to pay AMF
    And select 'Use Certified Membership Voucher' on checkout
    And enter voucher key which is not AMF related
    Then error message with "Incorrect voucher type" is received
     
  Scenario: Error type of Voucher redeemed for incorrect product
    Given person has passed exam
    And member services has approved OE
    When person navigate to member dashboard to pay AMF
    And select 'Use Certified Membership Voucher' on checkout
    And enter voucher key which is AMF related, but is NOT the correct type
    Then error message with "Voucher redeemed for incorrect product" is received
    
  Scenario: Error type of No active or pending subscriptions found
    Given contact had professional membership
    And now subscription has been changed to suspended
    When person navigate to member dashboard to pay AMF
    And select 'Use Certified Membership Voucher' on checkout
    And enter valid voucher key
    Then error message with "No active or pending subscriptions found" is received
    
  Scenario: Error type of Contact has already paid
    Given contact has already paid AMF using voucher through dashboard
    And has subscription created in pending status
    But sync betwen sitecore and Salesforce didn't go through
    When contact try to pay AMF again using same voucher through dashboard
    Then error message with "Contact has already paid" is received
    
  Scenario: Error type of You must specify an Item Type
    Given contact has passed exam
    And member services has approved OE 
    When person navigate to member dashboard to pay AMF
    And select 'Use Certified Membership Voucher' on checkout
    And enter valid voucher code
    But during API call item type is null
    Then error message with "You must specify an Item Type" is received