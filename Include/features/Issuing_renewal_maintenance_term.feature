Feature: Issuing renewal maintenance term
  As a member, I want my credential to renew automatically so that I can continue to use my credential immediately

  Scenario: Customer is an Associate of ISC2
    Given has an active maintenance term for Associate
    And current date has passed maintenance term end date
    And all goals are marked complete on current maintenance term
    When batch job is executed manually
    Then new maintenance term is created with current program term checkbox marked true
    And existing maintenance term's current program term checkbox is unchecked
    
  Scenario: Customer is a member of ISC2 for full certificates
    Given has an active maintenance term for full certificate
    And current date has passed maintenance term end date
    And all goals are marked complete on current maintenance term
    When batch job is executed manually
    Then new maintenance term is created with current program term checkbox marked true
    And existing maintenance term's current program term checkbox is unchecked
    
  Scenario: Customer is a member of ISC2 for full certificate with concentration
    Given has an active maintenance term for concentration
    And current date has passed maintenance term end date
    And all goals are marked complete on current maintenance term
    When batch job is executed manually
    Then new maintenance term is created with current program term checkbox marked true
    And existing maintenance term's current program term checkbox is unchecked
