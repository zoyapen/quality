Feature: Election eligible member has access to Survey & Ballots site
  As an election eligible member, I want to have access to reach the Survey & Ballots site and cast my vote

  Scenario: Member is election eligible
  Given member has election eligible flag marked true
  When member try to access elections voting web page
  Then they are passed through Survey and Ballots site
  
  Scenario: Member is Non-election eligible 
  Given member has election eligible flag marked false
  When member try to access elections voting web page
  Then they are redirected to a Not Eligible page
