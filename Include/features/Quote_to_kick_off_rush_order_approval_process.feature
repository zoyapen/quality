Feature: Quote to kick off rush order approval process

  Scenario:
    Given Quotes that require Rush order approval process should kick when criteria matches and submitted for approval
    And User create a B2B opportunity
    And User adds an Instructer led product with ship by date less than 30 days
    When User submits the quote for approval
    Then Rush order approval is triggered
    
  Scenario:
    Given Quotes that require Rush order approval process should kick when criteria matches and submitted for approval
    And User create a B2B opportunity
    And User adds an Lol class product with ship by date less than 30 days
    When User submits the quote for approval
    Then Rush order approval is triggered
    
  Scenario:
    Given Quotes that require Rush order approval process should kick when criteria matches and submitted for approval
    And User create a B2B opportunity
    And User adds an Kit product with ship by date less than 14 days
    When User submits the quote for approval
    Then Rush order approval is triggered
