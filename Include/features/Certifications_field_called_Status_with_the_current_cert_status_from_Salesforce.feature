Feature: Certifications field called Status with the current cert status from Salesforce 
 

  Scenario: Salesforce send Certification "Status" with each cert
    Given Salesforce send an updated
    And Contact has active Certifications
    When CIAM receives the data within the JSON
    Then Certifications will have a field called "Status" with the current cert
