Feature: Verify that button classes are replaced with new standard button classes

  Scenario Outline: Check whether Continue button on member-cpes have the new standard button class
    Given I'm on Member-CPES page
    When I inspect Continue button 
    Then I see the new standard button class for the continue button
 

  Scenario Outline: Check whether Pay buttons on member-dashboard-dues have the new standard button class
    Given I'm on Member-Dashboard-Dues page
    When I inspect all the <payButtons> on Member-Dashboard-Dues page
    Then I see the new standard button class for the Pay button
    Examples:
    |payButtons                                                 |
    |Pay now to become a member - Certified Membership Fee, 2020|
    |Pay on or before - Certified Membership Fee, 2020          |
    |Past Due - Certified Membership Fee, 2020                  | 

  Scenario Outline: Check whether slider buttons on member-preferences have the new standard button class 
    Given I'm on Member-Preferences page
    When I inspect all the <sliderButtons> on Member-Preferences-Page
    Then I see the new standard button class for the slider button
    Examples:
      |sliderButtons                                  |
      |Certifications, Education Resources and Offers |
      |Continuing Education & Professional Development|
      |Member Perks                                   |
      |News and Resources                             |
      |The Center For Cyber Safety and Education      |
      |Google Authenticator                           |
      |SMS Message                                    |
      |Question & Answers                             |

  Scenario: Check whether save button on member-profile-edit have the new standard button class 
    Given I'm on Member-Profile-Edit page
    When I inspect the save button
    Then I see the new standard button class for the save button
