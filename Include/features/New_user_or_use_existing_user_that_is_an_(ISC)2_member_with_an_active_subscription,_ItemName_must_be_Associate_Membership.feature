Feature: New user or use existing user that is an (ISC)2 member with an active subscription, ItemName must be Associate Membership 

  Scenario:New user or use existing user that is an (ISC)2 member with an active subscription, ItemName must be Associate Membership - Annual Fee, Today's date must be less than 45 days prior to the SubscriptionPaidThroughDate, Must have endorsement request with RequestStatus as Membership Complete within the last 9 months when navigates to Member dues page, User should be allowed to pay upgrade fee of $75
    Given A user has an Active subscription
    And ItemName must be Associate Membership - Annual Fee
    And Today's date must be less than 45 days prior to the SubscriptionPaidThroughDate
    And Must have endorsement request with RequestStatus as Membership Complete within the last 9 months when navigates to Member dues page
    When User navigates to the membership dues page
    Then user should be allowed to pay upgrade fee of $75
    
  Scenario: New user or use existing user that is an (ISC)2 member with an active subscription, ItemName must be Associate Membership - Annual Fee, Today's date must be greater than 45 days prior to the SubscriptionPaidThroughDate but less than 90 days after the SubscriptionPaidThroughDate, Must have endorsement request with RequestStatus as Membership Complete within the last 9 months when navigates to Member dues page, User should not be allowed to pay upgrade fee of $75
    Given A user has an Active subscription
    And ItemName must be Associate Membership - Annual Fee
    And Today's date must be greater than 45 days prior to the SubscriptionPaidThroughDate but less than 90 days after the SubscriptionPaidThroughDate
    And Must have endorsement request with RequestStatus as Membership Complete within the last 9 months when navigates to Member dues page
    When User navigates to the membership dues page
    Then user should not be allowed to pay upgrade fee of $75