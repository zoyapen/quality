Feature: Requests sent to this api will insert a record in SyncContactsV9 collection in MongoDB
  <mule app which accepts requests from isc2-usersync-api, transforms them, and creates, updates, and deactivates/deletes records in MongoDB>

  Scenario Outline: Insert a record in SyncContactsV9 collection in MongoDB using API
    Given Collection has all the required "Requests"
    When <apiRequests> are sent to MongoDB
    Then correct <OperationType> will be returned
    And record inserted into MongoDB
 
    Examples:
    |   apiRequests        | OperationType |
    | Post Create User     |    0          |
    | Put Update User      |    1          |
    | Delete User          |    2          |
    | Get User by LegacyID |   0/1         | 
    | Get User             |    1          |
 
 