Feature: Maintenance term display on member dashboard 
  As a Member, I need to see that my Dashboard displays the years of the term for my cycle instead of just having a generic Year 1, Year 2, Year 3 so that I know my membership cycle.

  Scenario: Member has active maintenance term
    Given person is a member of ISC2
    And has active maintenance term of 3 years
    When member navigate to Dashboard
    Then three years of the term are displayed clearly in Month Year format
