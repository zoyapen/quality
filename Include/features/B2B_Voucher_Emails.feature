Feature:B2B Voucher Emails
Given Asset record for vouchers has been generated 

Scenario Outline: Send <Voucher> emails
  
When  from the Asset record 'Send Voucher Codes' checkbox checked by sales rep
Then email sent from  info@isc2.org to the Shipping POC on the Quote and CC the Salesperson
And the email should be recorded as activity on the account
And the voucher email template should be specific to the type of vouchers being sent
And the voucher attachment should have the correct voucher code information, always include the voucher expiration date and voucher type.

Examples:

|Voucher|

|Associate Membership - Annual Fee|

|Professional Membership - Annual Fee|

|CISSP CBT Voucher|

|ISSEP CBT Voucher|

|HCISPP CBT Voucher|

|ISSAP Instructor-led Training Voucher|

|CAP Instructor-Led Training Voucher|

|HCISPP Instructor-led Training Voucher|



  Scenario:
    <Some interesting scenario steps here>
