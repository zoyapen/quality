Feature: Verify that GeoIPTest component located in Sitecore does not exist

  Scenario: Check whether GeoIPTest component is removed
    Given I log into Sitecore
    When I navigate to "/sitecore/content/TestWebsite/Home/"
    Then I do not see GeoIPTest component

 

 