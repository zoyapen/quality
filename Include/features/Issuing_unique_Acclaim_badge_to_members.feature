Feature: Issuing unique Acclaim badge to members
  As a member services associate, I should be able to issue unique Acclaim badge to members.

  Scenario: Person becomes member of ISC2
    Given member has all goals marked complete on Candidacy term
    And new maintenance term is issued
    When member services associate follows manual steps for acclaim badge creation
    Then new members are issued unique acclaim badges
