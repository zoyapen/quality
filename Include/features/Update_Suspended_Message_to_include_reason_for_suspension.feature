Feature: Update Suspended Message to include reason for suspension

  Scenario: Update Suspended Message to include reason for suspension
    Given Users subscription is in suspended state
    And User navigates to the web
    When User logs to the account on web
    Then user should see suspended message Your account is suspended due to failure to pay overdue AMF(s). To reactivate your account, please click <here to contact member services or call: +1.866.331.4722.
