Feature: AMF Invoices after suspension and during reinstatement

Scenario: Suspended member reinstates
Given Member is suspended  and has an invoice with  Overdue Status
When Member pays his fees online
Then Receipt is created with reference to the Overdue  Invoice  
And Invoice  status changed to Paid
