Feature: Suspended Member is shown that they are suspended on the Web Dashboard

  Scenario:Suspended Member is shown that they are suspended on the Web Dashboard
    Given A member subscription is in suspended status
    And member navigates to the web http://integration.isc2.org/ sing the same email listed on the Contact for that subscription
    When member is on the member dues dashboard http://integration.isc2.org/dashboard/memberdues
    Then member should see "Your account is suspended. To reactivate your account, please call member" message
