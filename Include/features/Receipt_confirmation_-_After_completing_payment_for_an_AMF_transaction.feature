Feature: Receipt confirmation - After completing payment for an AMF transaction

  Scenario: Once the successful payment is done, member should be directed to payment confirmation receipt
    Given User has member dues need to be paid
    And User navigates to member dues pages
    And user clicks on pay with credit card
    When user enters all required CC information and hits submit
    Then User gets redirected to payment confirmation receipt page
