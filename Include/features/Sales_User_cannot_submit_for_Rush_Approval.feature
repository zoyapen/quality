Feature:Sales User cannot submit for Rush Approval

  Scenario:Sales users should be able to submit a Quote for Rush Order Approval
    Given A sales user will be a submitting a quote for rush approval
    And A quote is created and a product that requires a rush approval is added
    And Ship by date is entered on fulfillment editor to qualify rush approval
    When The quote is submitted for approval
    Then The Quote is submitted for Rush approval
