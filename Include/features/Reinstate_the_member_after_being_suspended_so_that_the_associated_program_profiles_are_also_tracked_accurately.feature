Feature: Reinstate the member after being suspended so that the associated program profiles are also tracked accurately

  Scenario Outline: Reinstate the suspended <Program1> and make the associate <status><Status1> get updated accurately and also the reinstatement date
    Given User is on the subscription that is suspended
    And navigates to the contact record and navigates to the related list
    And validates the <Program1> have the <status><status1>
    When user changes the subscription back to Active
    Then only the program <status> changes to Maintenance and <status1> remains same
    And navigates to the contact reinstatement date
    Then the date is updated to the Reactivation date
    
    
    |Examples|
    | Program1|
    | Certification|
    | Concentration|
    | Associate|
    | Status|
    | Candidate|
    | Maintenance|
    | Upgraded|
    | Status1|
    | Banned|
    | Incomplete|
