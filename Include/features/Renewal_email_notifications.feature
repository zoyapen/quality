Feature: Renewal email notifications
As a member, I should receive renewal notification emails when my term gets renewed.

Scenario: Contact gets renewal maintenance term
Given member has active maintenance term
And all goals are marked complete on maintenance term
When scheduled renewal term batchjob is executed 
Then renewal maintenance term is issued
When scheduled renewal email batchjob is executed
Then new renewal notification email is sent
And email is logged in the activity history 


Scenario: Professional member with concentration gets renewal maintenance term
Given member has active maintenance term for concentration & full certificate
And all goals are marked complete on maintenance term for both
When scheduled renewal term batchjob is executed 
Then renewal maintenance term is issued for concentration & full certificate
When scheduled renewal email batchjob is executed
Then new renewal notification emails are sent
And emails are logged in the activity history

Scenario Outline: Candidate getting first maintenance term
Given customer has passed the exam for <MembershipType>
When all the goals are marked complete on candidacy term
Then new maintenance term is issued
And congratulatory email is send out
And emails are logged in the activity history

Examples:
|MembershipType|
|Professional|
|Associate|
|Concentration|
