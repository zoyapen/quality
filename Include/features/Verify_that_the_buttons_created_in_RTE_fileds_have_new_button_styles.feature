Feature: Verify that the buttons created in RTE fields have new button styles

  Scenario: Check whether the buttons created in RTE fields have new button styles
    Given I log into sitecore CM
    And I choose an item under the ISC node that has an RTE field
    When I click the show editor button of the RTE field 
    Then I see the buttons created in RTE fields have new button styles
  


  