Feature: Multi-Factor Authentication (MFA) disign matches in QA
 

  Scenario: Multi-Factor Authentication for Google Authenticator
    Given Sign In to "https://wwwqa.isc2.org"
     And clicking on "Preferences" 
     And activating "Multi-Factor Authentication" - "Google Authenticator"
     When re-Sign In "https://wwwqa.isc2.org" with the same account
     Then "Multi-Factor Authentication" page displays for "Google Authenticator"
      And page matches design <http://integration.isc2.org:8080/mfa-login.html>
     When "Google Authenticator" is successful 
     Then "Sign In" is successful
     
  Scenario: Multi-Factor Authentication for SMS Message
    Given Sign In to "https://wwwqa.isc2.org"
     And clicking on "Preferences" 
     And activating "Multi-Factor Authentication" - "SMS Message"
    When re-Sign In "https://wwwqa.isc2.org" with the same account
    Then "Multi-Factor Authentication" page displays for "SMS Message"
     And page matches design <http://integration.isc2.org:8080/mfa-login.html>
    When "SMS Message" is successful 
    Then "Sign In" is successful
     
  Scenario: Multi-Factor Authentication for Questions & Answers
    Given Sign In to "https://wwwqa.isc2.org"
     And clicking on "Preferences" 
     And activating "Multi-Factor Authentication" - "Questions & Answers"
    When re-Sign In "https://wwwqa.isc2.org" with the same account
    Then "Multi-Factor Authentication" page displays for "Questions & Answers"
     And page matches design <http://integration.isc2.org:8080/mfa-login.html>
    When "Questions & Answers" is successful 
    Then "Sign In" is successful