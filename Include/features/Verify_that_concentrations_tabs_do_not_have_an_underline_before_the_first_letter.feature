Feature: Verify that concentrations tabs do not have an underline before the first letter

  Scenario: Check whether the concentrations tabs do not have an underline before the first letter
    Given I navigate to CISSPConcentrations
    Then I see that concentrations tabs do not have an underline before the first letter
