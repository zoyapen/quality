Feature: Create Salesforce Contact Adapter to Read users contacts from CIAM Bus and into Salesforce

"""The purpose of the Salesforce Adapter is to read new or updated contact records from the service
bus and write the data to Salesforce.  Upon reading the data from the service bus, the most 
basic validation is performed to ensure that a ContactId and Email address are present.  After 
that the adapter authenticates against Salesforce OAuth2, supplying the required data attributes 
of ClientId, ClientSecret, UserName, and Password.  If authentication fails, the adapter writes 
a message to the AdapterFailure Mongo collection with the error message and other attributes 
that indicate which adapter had the error and when it occurred.  If authentication was 
successful, the Salesforce Adapter will attempt to make a REST API call to custom Salesforce 
Apex end point that will perform additional validation on the contact record, and make a 
determination if the record is to be inserted or updated in Salesforce.  If there are any issues
with writing the contact record to Salesforce, the error will be logged in the AdapterFailure 
mongo collection.   The Service Bus and its adapters are not responsible for any validation of 
Contact Records besides the presence of the ContactId and Email address.  It is the 
responsibility of the Sitecore WWW and Salesforce to ensure field lengths, character sets, text 
formats validation are performed as needed."""

@www-2469

Scenario:Salesforce Adapter - (Happy Path) Create New Account in WWW site
  
  # newAccountData = First Name, Last Name, Email, Confirm Email, Password, Confirm Password
  # allSytems = WWW, Okta, LMS, Course Merchant, CRM
  # accountData = First Name, Last Name, Email
  
    Given user has navigated to WWW (ISC) "Create a New (ISC) User Account" page
     And completed required <newAccountData> fileds 
     And clicking the checkbox to agree to terms for "Privacy Policy"
    When the user clicks the button to "CREATE A NEW ACCOUNT"
    Then WWW Site will validate that all fields are present and valid
     And create a "ContactId" that will uniquely identify the contact in <allSystems>
     And Sitecore will add the "contactRecord" to the service bus where it will be picked and processed by the "SalesforceAdapter"
    When "SalesforceAdapter" reads the message from the message bus it will attempt an "Upcert", the first attempts will be to locate a contact record by the "ContactId"
     And if the "contactRecord" does not exist a contact record will be inserted
     And if a contact is found "contactRecord" will be updated
    Then Salesforce contact "contactRecord" will have the same values as submitted via WWW (ISC) site for <accountData>
     And Salesforce contact will have a uniquely identify "ContactId"
