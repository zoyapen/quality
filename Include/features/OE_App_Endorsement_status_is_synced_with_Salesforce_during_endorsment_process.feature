Feature: OE App Endorsement status is synced with Salesforce during endorsment process
  

  Scenario: Process Endorsement Request via Salesforce that sync status to OE App
    Given Candidate has Pass Exam for Certification
    When Candidate submits Endorsement request via OE App
    Then Salesforce status for that application indicates "Your application has been received but processing by (ISC)² has not yet started." 
    When application has been "Completed"
    Then OE App show synced status of "Congratulations! Your application has been approved. You are now a <Certification>"