Feature: CISSP certification goal marked completed on concentration during renewal 
  As a member, once I complete all goals for my CISSP credential, CISSP certification program goal on Program Term for concentration is marked complete.

  Scenario: Member has active CISSP with incomplete goals
    Given member has active maintenance term for CISSP & concentration
    And all program goals are incomplete for both
    And program term end date is within 45 days
    When member pay AMF through portal
    Then membership goal is set complete on CISSP maintenance term
    And membership goal is set complete on concentration maintenance term
    And CISSP certification goal is incomplete on concentration maintenace term
    When member submits CPEs to satisfy minimum requirement of CISSP credential
    Then CPE program goals are set to complete on CISSP maintenance term
    And CISSP certification goal is complete on concentration maintenace term
    
