Feature: Member - Sales Order and Sales Order Line Creation Permissions

  Scenario: User should be able to update subscription while accessing the AMF link to pay dues
    Given User subscription is still in grace period
    And User navigates to the Site
    When User is on the Pay AMF link
    Then user should be able to update subscription and add products to shopping cart for payment
