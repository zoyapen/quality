Feature: Verify that the indentation is fixed in the reset password input field

  Scenario: Check whether the indentation is fixed in the rest password input field
    Given I navigate to the sign in page
    And I click the forget your password link 
    And I enter a valid email address
    And I request new password
    And I open the mailbox 
    When I click the reset password link in the e-mail
    Then I see the correct indentation for reset password input field on reset password page