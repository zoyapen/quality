Feature: Once a Quote is Docusigned the copy is available to user as a PDF

  Scenario:Once a Quote is Docusigned the copy is available to user as a PDF
    Given A quote is approved and sent for Docusign
    And customer receives the Docusign
    When Customer completes the Docusign
    Then On the Quote related list, A Docusign PDF is created
