Feature: Automatic Status Change after payment made by Suspended Member
  As an ISC2 customer that was previously suspended, my subscription should be set back to Active after AMF payment has been made.

  Scenario Outline:Suspended Member with a Subscription Paid Through Date equal to the current date
    Given Contact has <SubscriptionType> in suspended status
    When Member pays AMF
    And Subscription Paid Through Date is equal to the currenrt date
    Then Subscription Status reads as Active
 
  Examples:
    |Current Date: 10/31/2019|
    |Subscription Paid Through Date: 10/31/2019|
    |Status: Active|

  Scenario Outline: Suspended Member with a Subscription Paid Through Date after the current date
    Given: Contact has <SubscriptionType> in suspended status
    When Member pays AMF
    And Subscription Paid Through Date is after the current date
    Then Subsciption Status reads as Active
    
    Examples:
      |Current Date: 10/31/2019|
      |Subscription Paid Through Date: 9/30/2020|
      | Status: Active|