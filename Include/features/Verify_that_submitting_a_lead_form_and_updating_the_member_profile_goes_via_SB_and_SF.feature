Feature: Verify that submitting a lead form and updating the member profile goes via SB and SF
  
  Scenario: Verify that submitting a lead form is sent to SB and then comes to SB from SF
    Given I navigate to "https://wwwqa.isc2.org/Training/Private-On-Site"
    And I enter the required details 
    And I submit the lead form
    And I log into CIAM
    And I navigate to contact log
    Then I see the lead form is sent to SB 
    And I log into SF
    When I navigate to leads
    Then I see the above lead details in salesforce

  Scenario: Verify that editing member profile is sent to SB and SF and a response comes back to SB from SF
    Given I log into Sitecore
    And I navigate to member profile page
    And I edit the member profile details
    Then I see the edit is successful
    And I log into CIAM
    And I navigate to contact log
    Then I see the updated member profile is sent to SB 
    And I see the response comes back to SB from SF
    And I log into SF
    When I navigate to contacts
    And I look up the details for the above contact
    Then I see the above updated information in salesforce