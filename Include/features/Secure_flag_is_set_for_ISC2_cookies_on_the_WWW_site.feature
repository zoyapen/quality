Feature: Secure flag is set for ISC2Int and ISC2Int1 cookies on the WWW site
  
  Scenario Outline: While accessing isc2 site cookies are set with SecureFlag
    Given I have navigated to "ISC2" web site
    When I authenticate and access various "ISC2" pages
    Then my activites should be protected with "ISC2Int" and "ISC2Int1" cookies enabled 
    And <protectedActions> for the following:

  Examples:
    |protectedActions|
    | deny access to the headers through XMLHttpObject |
    | restricts all access to document.cookie |
    | no cross-domain posting of the cookies |
    | cookie will only be sent over HTTPS |