Feature: Aware of the current status of endorsement application when status is Pending ISC2 Assistance
 

  Scenario: OE App status is correct when Salesforce ER status is 'Pending ISC2 Assistance'
    Given OE Application has been submitted with "Request (ISC)² to endorse you."
    When Endorsement Request is recevied by Salesforce
    And "Assistance Application Review Status" is set to <Recommended>
    And ER "Request Status" is set to <Pending ISC2 Assistance>
    Then OE App status displays "Your application is being reviewed by (ISC)2 for Endorsement Assistance"
    When ER Case is resolved
    And moved to Approved
    Then ER "Request Status" is set to <Membership Complete>
    And OE App display "Congratulations! Your application has been approved..."
    
    
