Feature: Opportunity moved to closed won stage
  As a finance user, I should see correct Order Start date on Order. Order start date should be set to the date the opportunity moves to any closed won stage.

  Scenario Outline: Opportunity is moved to closed won stage
    Given <OpportunityType> opportunity is created
    When it is moved to Closed Won stage
    Then Order start date field on Order is set to today's date
    
    Examples:
      |OpportunityType|
      | B2B|
      | B2c|
