Feature: Renewal email notifications for professional members
As a member, I should receive renewal notification emails when my term gets renewed.

Scenario: Professional member gets renewal maintenance term
Given member has active maintenance term
And all goals are marked complete on maintenance term
When scheduled renewal term batchjob is executed 
Then renewal maintenance term is issued
When scheduled renewal email batchjob is executed
Then new renewal notification email is sent
And email is logged in the activity history 

Scenario Outline: Candidate getting first maintenance term
Given customer has passed the exam with Professional membership intent
When all the goals are marked complete on candidacy term
Then new maintenance term is issued
And congratulatory email is send out
And emails are logged in the activity history
When scheduled renewal term batchjob is executed
Then no renewal email goes out