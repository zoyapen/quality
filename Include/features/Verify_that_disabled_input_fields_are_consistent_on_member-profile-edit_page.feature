Feature: Verify that disabled input fields are consistent on member-profile-edit page
  
  Scenario Outline: Check whether the disabled input fields on member-profile-edit page for personal information are consistent
    Given I'm logged into Sitecore
    And I navigate to member profile page
    When I edit the personal information 
    Then I see the disabled <inputFields> are consistent 
    Examples:
        |inputFields    |
        |Primary Email  |
        |First Name     |
        |Middle Name    |
        |Last Name      |
        
  Scenario Outline: Check whether the disabled input fields on member-profile-edit page for personal information are consistent for a member with middle name
    Given I'm a member with middle name
    And I'm logged into Sitecore
    And I navigate to member profile page
    When I edit the personal information 
    Then I see the disabled <inputFields> are consistent 
    Examples:
        |inputFields    |
        |Primary Email  |
        |First Name     |
        |Middle Name    |
        |Last Name      |

  Scenario Outline: Check whether the disabled input fields on member-profile-edit page for personal information are consistent for a member without a middle name
    Given I'm a member without a middle name
    And I'm logged into Sitecore
    And I navigate to member profile page
    When I edit the personal information 
    Then I see the disabled <inputFields> are consistent 
    Examples:
        |inputFields    |
        |Primary Email  |
        |First Name     |
        |Middle Name    |
        |Last Name      |
