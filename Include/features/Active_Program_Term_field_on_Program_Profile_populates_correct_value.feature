Feature: Active Program Term field on Program Profile populates correct value
  As a member services associate, I should see maintenance term with current term set to true in active program term field. 

  Scenario: Contact in candidate status with multiple terms marked current term
    Given contact has passed exam 
    And is issued candidacy term
    When multiple program term are marked with current term as true
    Then last current term stamped in active program term field
    
  Scenario: Contact in maintenance status with multiple terms marked current term
    Given contact is issued maintenance term
    When multiple program term are marked with current term as true
    Then last current term stamped in active program term field
    
  Scenario: Contact in candidate status with single term marked current term
    Given contact has passed exam 
    And is issued candidacy term
    When single program term is marked with current term as true
    Then active program term field on Program Profile is populated with correct term

  Scenario: Contact in candidate status with single term marked current term
    Given contact is issued maintenance term
    When single program term is marked with current term as true
    Then active program term field on Program Profile is populated with correct term