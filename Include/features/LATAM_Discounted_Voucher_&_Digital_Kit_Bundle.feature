Feature:LATAM Discounted Voucher & Digital Kit Bundle

  Scenario:
    Given Digital kits LATAM is avaiable as a selection for LATAM user
    And An account is created with Region as LATAM
    When user is adding a product
    Then The 'Digital Kits LATAM' is available for selection
    
 
  Scenario:
    Given Digital kits LATAM is not avaiable as a selection for NAR user
    And An account is created with Region as NAR
    When user is adding a product
    Then The 'Digital Kits LATAM' is not available for selection