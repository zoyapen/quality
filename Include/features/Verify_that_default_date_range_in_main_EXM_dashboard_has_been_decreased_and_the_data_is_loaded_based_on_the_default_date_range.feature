Feature:Verify that default date range in main EXM dashboard has been decreased and the data is loaded based on the default date range
  
  Scenario: Verify that default date range in main EXM dashboard has been decreased and the data is loaded based on the default date range
    Given I'm on Sitecore CM
    And I navigate to Email Experience Manage
    Then I see an expected default date range
    And I see the data is loaded in the dashboard