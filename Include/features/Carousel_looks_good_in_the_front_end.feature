Feature: Carousel looks good in the front end for the following window size, width 360 px, 768 px and 1200 px

    Scenario Outline: Check whether all the carousel looks good in the front end for a non-logged in user
        Given I'm on <browser>
        And I navigate to "https://wwwqa.isc2.org/"
        Then I see there is not too much vertical space at top of page for <carousel> 
        And I see the <carousel> arrows are semi-transparent and matches the bg or text color
            Examples:
              |browser|carousel  |
              |Chrome |Hero      |
              |Chrome |Media     |
              |Chrome |Membership|
              |Chrome |Timeline  |
              |IE     |Hero      |
              |IE     |Media     |
              |IE     |Membership|
              |IE     |Timeline  |
    
    Scenario Outline: Check whether the carousel looks good in the front end for a member-user
        Given I'm on <browser>
        And I'm logged in as a "memberUser"
        And I navigate to "https://wwwqa.isc2.org/"
        Then I see there is not too much vertical space at top of page for <carousel> 
        And I see the <carousel> arrows are semi-transparent and matches the bg or text color
            Examples:
              |browser|carousel  |
              |Chrome |Hero      |
              |Chrome |Media     |
              |Chrome |Membership|
              |Chrome |Timeline  |
              |IE     |Hero      |
              |IE     |Media     |
              |IE     |Membership|
              |IE     |Timeline  |
        
    Scenario Outline: Check whether the carousel looks good in the front end for a prospect-candidate
        Given I'm on <browser>
        And I'm logged in as a "prospectCandidate"
        And I navigate to "https://wwwqa.isc2.org/"
        Then I see there is not too much vertical space at top of page for <carousel> 
        And I see the <carousel> arrows are semi-transparent and matches the bg or text color
            Examples:
              |browser|carousel  |
              |Chrome |Hero      |
              |Chrome |Media     |
              |Chrome |Membership|
              |Chrome |Timeline  |
              |IE     |Hero      |
              |IE     |Media     |
              |IE     |Membership|
              |IE     |Timeline  |