Feature: <Opportunity> stages are updated automatically when the appropriate stages are changed/updated

  Scenario Outline:: <Opportunity> stages are updated automatically when the appropriate stages are changed/updated
    Given Quote is created on the opportunity
    And User adds a voucher product and is submitted for approval
    And the quote is approved
    And user sends the docusign to the customer
    When customer completes docusign
    Then the <Opportunity> stage is changed to Agreement signed
    And user takes the Payment for the Quote
    Then the <Opportunity> stage is updated to Payment received

    Examples:
        |Opportunity|
        | B2C|
        | B2B|