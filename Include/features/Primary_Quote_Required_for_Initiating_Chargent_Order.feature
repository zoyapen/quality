Feature:Primary Quote Required for Initiating Chargent Order

  Scenario:
    Given for any B2C chargent order a Primary Quote is required
    And A B2C Opportunity and quote is created
    When Quote is Approved
    Then The new Chargent order button is available
    
    Scenario:
    Given for any B2C chargent order a Primary Quote is required
    And A B2C Opportunity and quote is created and approved
    When Quote status is changed from approved to draft
    Then The new Chargent order button is not available
