Feature: Previously entered labels and credits should not appear as an autofill dropdown

  Scenario: Member reports CPE
    Given Member navigates to next 'Category & Detail' Page
    When Member enters detail on the Category&Detail Page
    Then No autofill dropdowns visible for labels, credits an d other fields
