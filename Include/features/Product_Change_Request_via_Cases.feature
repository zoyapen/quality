Feature:Product Change Request via Cases

  Scenario:Education Manager need to have a formal place to request product and pricing additions and change
    Given Education wants to submit a case requesting to change Catalog/pricing
    And Education submits a case requesting a change
    When Finace makes the changes
    Then Make sure the automation works as expected and Finance, Education gets notified of Catalog/Price updates
