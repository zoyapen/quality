Feature: Member - Ability to link to Pay AMF within the cycle from the Dashboard

  Scenario: Member - Ability to link to Pay AMF from the Dashboard when SubscriptionPaidThroughDate is within 45 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is within 45 days from todays date
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then member should see Pay With Card and Pay With Voucher should be visible and Total amount due should be $125
    
  Scenario: Member - Ability to link to Pay AMF from the Dashboard when SubscriptionPaidThroughDate is within 90 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is within 90 days from today's date
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then member should see Pay With Card and Pay With Voucher should be visible and Total amount due should be $125
  
Scenario: Member should not see any member dues in the dashboard when SubscriptionPaidThroughDate is prior to 45 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is Prior to 45 days from todays date
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then Member should not see any link to pay dues
    
  Scenario: Member should not see any member dues in the dashboard when SubscriptionPaidThroughDate is more than 90 days from today's date 
    Given User is on the Member subscription that is in Active Status
    And Item (Primary) is Professional Membership - Annual Fee
    And Subscription Paid Through Date is more than 90 days from today's date
    And member logs into web (http://integration.isc2.org/) same email on the contact for that subscription
    When member navigates to member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then Member should not see any link to pay dues
  
  Scenario:  Non member should not see any member dues in the dashboard
    Given a Non member navigates to http://integration.isc2.org/
    And member navigates to the member dues dashboard (http://integration.isc2.org/dashboard/memberdues)
    Then Member should not see any link to pay dues
    
    