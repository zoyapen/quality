Feature: Verify that 

Scenario Outline: Verify that functionality for the endpoints with https that work should return a correct response
    Given I launch Postman
    And I import the collection named "Area- CIAM APIs - Create implemented.postman_collection"
    And I execute the following "requests"
    Then I see a valid response for the above request
       Examples:
         |requests                      |
         |POST D2L - Create User        |
         |POST MDB - Create User        |
         |Pearson - CDD                 |
         |POST Okta - Create User       |
         |POST SF - Create Contact      |
         |POST UserAsync - Create User  |
         |GET - Updated Flags           |
         |GET UserAsync - Ping Service  |

    Scenario Outline: Verify that functionality for the endpoints with https that are not yet implemented should return method not implemented in the response
    Given I launch Postman
    And I import the collection named "Area- CIAM APIs - Create implemented.postman_collection"
    And I execute the following "requests"
    Then I see "Method not implemented"
       Examples:
         |requests                              |
         |GET SF - Get Contacts                 |
         |GET SF - Get Contact by Id            |
         |PUT SF - Update Contact by Id         |
         |DEL SF - Delete Contact by Id         |
         |GET Okta - Get Users                  |
         |GET Okta - Get User by Id             |
         |PUT Okta - Update User by Id          |
         |DEL Okta - De-ActivateUser by Id      |
         |GET Mdb - Get Users                   |               
         |GET Mdb - Get User by LegacyId        |
         |PUT Mdb - Update User                 |
         |DEL Mdb - Delete User                 |
         |GET D2L - Get Users                   |
         |GET D2L - Get Users by Id             |
         |PUT D2L - Update User                 |
         |DEL D2L - Delete User                 |
         |GET User Sync - Get Users Sitecore    |
         |GET Users by Id Sitecore              |
         |GET User Sync - Get User by Id Sitecore |
         |PUT User Sync - Update User Sitecore    |
         |DEL User Sync - Delete User Sitecore    |
         |GET User Sync - Get User Salesforce       |
         |GET User Sync - Get User by Id Salesforce |
         |PUT User Sync - Update User Salesforce    |
         |DEL User Sync - Delete User Salesforce    |
    
    Scenario: Verify that APIs with http returns 504 status code
    Given I launch Postman
    And I import the collection named "Area- CIAM APIs - Create implemented.postman_collection"
    And I execute userAsync - ping service with http
    Then I see 504 status code
      