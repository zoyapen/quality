Feature: Verify that creating a contact via endpoint and verifying the contact has Salesforce Status of CREATED works as expected

  Scenario Outline: Verify that creating a contact via endpoint and verifying the contact has Salesforce Status of CREATED works as expected
    Given I have a <nonExistingEmail> in the request body of a POST request
    When I POST a contact for a non-existing email
    Then I see the contact record creation is successful
    And I see the correct statusCode, returnMessage, email and other contact details
    And I see the "CREATED" status for salesforce_Status__c
    When I log into Salesforce
    And I search for the above contact by email
    Then I see the correct contact in the results section
    And I click on the contact
    When I navigate to the related tab of the above contact
    And I scroll-down to Connected System Statuses
    And I click on the Id
    Then I see the Salesforce status as "CREATED"
    Examples:
      |nonExistingEmail         |
      |cmr2111211@mailinator.com|
      |cmr21112@mailinator.com  |

