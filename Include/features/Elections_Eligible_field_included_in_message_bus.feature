Feature: Elections Eligible field included in message bus
  

  Scenario: Member is election eligible
    Given member has elections eligible field on contact page 
    When field is marked true
    And wait for some time for sync to go through
    Then field is picked by message bus 
    And is visible updated in mongo db
