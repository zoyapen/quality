Feature:Next/Save&Continue button disabled until selection is made on the Domain Page

Scenario:Member clicks 'Save&Continue' without selecting any domain 
Given:Member is on Domain Page submitting new CPE
When Member clicks 'Save&Continue' button without selecting any domain
Then 'Save&Continue' button stays disabled, Member not able to navigate to next Page

Scenario:Member selects domain clicks 'Save&Continue' 
Given:Member is on Domain Page submitting new CPE
When Member selects domain
And  Member clicks 'Save&Continue' button
Then Member navigates to next Page - Review Page

