Feature: Next/Continue button disabled until two valid dates are entered
Background:
Given Member has an active maintenance term and access to New CPE Portal

Scenario: Member clicks 'Continue' button without entering any dates
When Member is on the Home Page of New CPE Portal
And Member clicks 'Continue' button without entering Dates
Then 'Continue' button stays disabled, Member not able to navigate to next Page

Scenario: Member enters only Start date 
When Member is on the Home Page of New CPE Portal
And Member enters Start date and clicks 'Continue'
Then 'Continue' button stays disabled, Member not able to navigate to next Page

Scenario: Member enters only End date
When Member is on the Home Page of New CPE Portal
And Member enters End date and clicks 'Continue'
Then 'Continue' button stays disabled, Member not able to navigate to next Page

Scenario: Member enters valid Start and invalid End dates 
When Member is on the Home Page of New CPE Portal
And Member enters valid Start date and invalid End date and clicks 'Continue'
Then 'Continue' button stays disabled, Member not able to navigate to next Page

Scenario: Member enters valid Start and End dates 
When Member is on the Home Page of New CPE Portal
And Member enters valid Start date and End date and clicks 'Continue'
Then Member navigates to next 'Category & Detail' Page


  | 
