Feature: Intermittently (daily) process to terminate any active (Benefit Status =1) 

  Scenario:
        Given an Active Transition/Base Benefit pair
        When the Base benefit is terminated
        Then the Transition benefit should also be terminated