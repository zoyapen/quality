Feature: threeTilesIcon component is working as expected located in home page with different browser widths

  Scenario Outline: Verify that threeTilesIcon component is working as expected located in home page with different browser widths
    Given I'm on sitecore QA
    And I see the <browserWidth>
    And I navigate to the home page
    And I scroll down to threeTilesIcon component
    And I click through different options
    Then I see clicking different options or tiles works as expected
        Examples:
          |browserWidth|
          |375px       |
          |768px       |
          |1024px      |
    
