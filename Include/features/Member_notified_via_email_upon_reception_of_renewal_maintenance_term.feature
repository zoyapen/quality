Feature: Member notified via email upon reception of renewal maintenance term
 
@AMS-1888

  Scenario Outline: Member gets renewal term for full certificate
    Given member has <Country> listed in billing country
    And member satisfies all goals for current maintenance term
    When member is issued a new renewal maintenance term
    Then renewal notification email is sent
    And email is logged in activity history
    And language of the email is <Language>
   
   Examples: 
    | Country | Language |
    | China   | Chinese  |
    | Korea   | Korean   |
    | Japan   | Japanese |
    
  Scenario Outline: Member gets renewal term for Associate
    Given member has <Country> listed in billing country 
    And member satisfies all goals for current maintenance term
    When member is issued a new renewal maintenance term
    Then renewal notification email is sent
    And email is logged in activity history
    And language of the email is <Language>
   
   Examples: 
    | Country | Language |
    | China   | Chinese  |
    | Korea   | Korean   |
    | Japan   | Japanese |

  Scenario Outline: Associates has maximum limit for getting new maintenance terms before getting converted into full member
    Given person becomes an Associate of ISC2 for <Program>
    When current maintenance term is about to expire
    And all relevant goals are marked as complete
    Then new renewal maintenance term is issued
    And 'Total No of Program Terms' field is incremented by 1
    And cycle continues till 'Total No of Program Terms' field value equals <MaxNumber>
   
   Examples: 
    | Program          | MaxNumber |
    | Associate CISSP  | 6         |
    | Associate SSCP   | 2         |
    | Associate CCSP   | 6         |
    | Associate CSSLP  | 5         |
    | Associate HCISPP | 3         |
    | Associate CAP    | 3         |