Feature: Program Profile suspension
  As a member services associate, I should see member's program profile marked suspended when related maintenance term has CPE goals incomplete and current date is more than 90 days of term end date

  Scenario Outline: Contact with single active Program Profile 
    Given contact has single <MembershipType> maintenance term
    And has CPE goals marked incomplete
    When current date has passed 90 days after maintenance term end date
    Then program profile marked suspended
    
  Examples:
    |MembershipType|
    | Full membership |
    | Associate |
    
  Scenario: Contact with multiple Profiles
    Given contact has multiple profile in maintenance status
    And has active maintenance term for each profile
    And has cpe goal marked incomplete on one maintenance term
    When current date has passed 90 days after maintenance term end date
    Then only Program Profile with incomplete cpe goal marked suspended
    
  Scenario: Member with concentration
    Given member has profile for CISSP and concentration in maintenance status
    And cpe goals are marked incomplete on CISSP maintenance term
    And cpe goals are marked complete on concentration maintenance term
    When current date has passed 90 days after CISSP maintenance term end date
    Then both program profile are marked suspended
    
    
    
