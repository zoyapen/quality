Feature: Allow Sales users to Recall Quote when the quote is in discount Approval or rush order approval process

    Scenario Outline: Allow Sales users to Recall Quote when the quote is Still in a discount Approval process
    Given Sales users Add a product (Book) to quote that can be discounted 
    And Sales users apply a discount of 15%
    When the quote is submitted for Approval
    Then the Sales users can navigate to approval history and recall the quote when it is still in Discount Approval process

    Scenario Outline: Allow Sales users to Recall Quote when the quote is still in a Rush Approval process
    Given Sales users Add a product (Book) to quote that qualifies for rush approval
    And Sales users enters the ship by date less than 14 days
    When the quote is submitted for Approval
    Then the Sales users can navigate to approval history and recall the quote when it is still in Rush Approval process