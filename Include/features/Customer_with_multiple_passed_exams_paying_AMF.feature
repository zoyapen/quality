Feature: Customer with multiple passed exams paying AMF
  As a customer, I should get option to pay when OE for my first exam is approved and within last nine months. 

  Scenario: Customer passed two exams
    Given person has passed exam result for two program
    And has approved OE within last 9 months for first exam
    And no OE submitted for second exam
    When person login to dashboard
    Then $125 AMF due visible 

 Scenario Outline: Customer passed exam for one program
   Given person passed exam with <MembershipType> intent
   And has OE submitted for professional passed exam type
   When person login to dashboard
   Then <Amount> AMF due visible
   
   Examples:
     |MembershipType|Amount|
     | Professional|$125|
     | Associate| $50|
     
  Scenario: Customer passed exam for Associate and Professional
    Given person passed exam with Associate intent
    And passed other exam with full membership intent
    And has approved OE within last 9 months for full membership intent credential
    When customer login to his dashboard
    Then $125 AMF due visible