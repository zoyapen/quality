Feature: Error massages for invalid dates entered
Background:
Given Member is on the Home Page of New CPE Portal

  Scenario Outline: Member enters dates in the future 
    When Member enters future <Start> and <End> dates 
    Then <Error massage> pop-ups and also appears by the applicable fields
   
Examples:
  |Start     |   |   End    |   |Error Massage               |
  |01.01.2020|   |01.03.2020|    |Date cannot be in the future|
  |12.25.2020|   |12.26.2020|    |Date cannot be in the future|
  
   Scenario Outline: Member enters End date that comes before Start date
    When Membere enters  <Start> and <End> dates 
    Then <Error massage> pop-ups and also appears by the applicable fields
Examples:
  |Start     |   |   End    |    |Error Massage            |
  |10.13.2020|   |10.01.2020|    |The start date must not be after the end date|
  |08.25.2020|   |08.13.2020|    |The start date must not be after the end date|
  
  
  Scenario Outline: Member enters dates in the invalid format
    When Member enters <Start> and <End> dates 
    Then <Error massage>appears by the applicable fields
   
Examples:
  |Start     |   |   End    |   |Error Massage               |
  |10.01.19  |   |10.03.19  |   |Your entry does not match the allowed format MMM d, yyyy.|
  |13.07.2019|   |13.07.2019|   |Your entry does not match the allowed format MMM d, yyyy.|
  
 