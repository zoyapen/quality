Feature: Suspended message displayed on contact page
  As a member services staff, when member has unpaid AMF for more than 90 of due date then subscripton is suspended and Suspended message is displayed on contacts page 

  Scenario: Unpaid AMF till end of grace period
    Given member has subscription
    And has AMF due
    When member is out of grace period
    Then subscription is changed to suspended
    And Program Profile is changed to suspended status
    And suspended message is displayed on Contacts page
    
