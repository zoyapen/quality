Feature: Verify that editing and saving form name in CMS is successful and does not throw any error

Scenario Outline: Verify that editing and saving form name in CMS is successful and does not throw any error
    Given I'm on CMS
    And I open content editor
    And I navigate to /sitecore/content/ISC2Flobal/Forms
    And I edit the <form> name
    And I save it 
    Then I see the edit was successful
    And I do not see any errors
      Examples:
        |form                |   
        |About-ISC2          |
        |Add Security ebook  |