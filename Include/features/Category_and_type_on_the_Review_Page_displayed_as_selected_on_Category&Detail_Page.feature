Feature:Type of CPE selected displayed on Review Page

  Scenario Outline: Member is adding new CPE
    Given Member is on the Home Page of NewCPEPortal
    When Member enters valid Start date and End date and clicks 'Continue'
    And Member selects <Category> and <Type> for CPE, clicks 'Save&Continue'
    And Member selects program domains and clicks 'Save&Continue'
    Then Member navigates to Review Page
    And Member can see under Category&Detail selected <Type>
   Examples:
  |                         Category                |   |             Type                                                                                                           |
  |                        Education               |   |(ISC)² 1-Day SecureEvent                        |
  |                         Education              |   |(ISC)² CISO Says Roundtable Webinar             |
  |                         Education              |   |(ISC)² e-Symposium Webinar                      |
  |                         Education              |   |(ISC)² In the Trenches Roundtable Webinar       | 
  |                         Education              |   |(ISC)² SecureSummit                             |     
  |                         Education              |   |(ISC)² Security Briefing Webinar                |
  |                         Education              |   |(ISC)² Security Congress                        |
  |                         Education              |   |(ISC)² ThinkTank Roundtable Webinar             |
  |                         Education              |   |(ISC)²'s InfoSecurity Professional Magazine     |
  |                         Education              |   |CPE Rollover                                    |
  |                         Education              |   |Education Courses and Seminars                  |
  | Contributions to the Profession|   |(ISC)² Chapter Formation                                        |
  | Contributions to the Profession|   | (ISC)² Chapter Management                                      |
  | Contributions to the Profession|   |ISC)² Chapter Meeting                                           |
  | Contributions to the Profession|   |(ISC)² Chapter Officer Meeting                                  |
  | Contributions to the Profession|   | ISC)² Exam Proctor                                             |
  | Contributions to the Profession|   |(ISC)² JTA Surveys                                              |
  | Contributions to the Profession|   |(ISC)² Professional Development Institute                       |
  | Contributions to the Profession|   |ISC)² Safe and Secure Volunteer Training                        |
  | Contributions to the Profession|   |Board Services for a Professional Security Organization         |
  | Contributions to the Profession|   |Education Item Writing Workshop                                 |
  | Contributions to the Profession|   |Education Item Writing Workshop                                 |
  | Contributions to the Profession|   |Government/Private Sector/Charitable Organizations Committees   |
  | Contributions to the Profession|   |Item Development Subject Matter Experts                         |
  | Contributions to the Profession|   |Prepare For Presentation for (ISC)² related event               |
  | Contributions to the Profession|   |Preparing New or Updating Existing Training Seminar or Classroom Material|
  | Professional Development         |   | Concentration CPE Adjustment      |