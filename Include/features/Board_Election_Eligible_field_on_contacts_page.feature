Feature: Board Election Eligible field on contacts page
  As a system Admin, I should see 'Board Election Eligible' field on contact page used for differentiating member who is eligible vs who is ineligible from participating into Board of Member Elections.

  Scenario: Board Election Eligible check box field added to contact object
    Given Contact is active member of ISC2
    When System Admin navigates to contact page
    Then 'Board Election Eligible' checkbox field visible
    When checbox is marked true
    Then update is captured by message bus
    And read access given to Sitecore Integration User, Customer Support User, and Product Owner profile
