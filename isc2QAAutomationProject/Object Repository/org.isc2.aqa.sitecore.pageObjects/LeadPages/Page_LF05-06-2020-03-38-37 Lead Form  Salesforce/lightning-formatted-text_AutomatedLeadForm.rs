<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lightning-formatted-text_AutomatedLeadForm</name>
   <tag></tag>
   <elementGuidId>820aafb9-7f2a-4030-b7f5-4b5adc3eece4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='brandBand_1']/div/div/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_lead_rec_l_lead__view_js/flexipage-record-page-decorator/div/slot/flexipage-record-home-with-subheader-template-desktop2/div/div/slot/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-highlights-panel/records-lwc-record-layout/forcegenerated-highlightspanel_lead___012000000000000aaa___compact___view___recordlayout2/force-highlights2/div/div[2]/slot/slot/force-highlights-details-item/div/p[2]/slot/lightning-formatted-text</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>lightning-formatted-text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AutomatedLeadForm</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;brandBand_1&quot;)/div[@class=&quot;slds-template__container&quot;]/div[@class=&quot;center oneCenterStage lafSinglePaneWindowManager&quot;]/div[@class=&quot;windowViewMode-normal oneContent active lafPageHost&quot;]/div[@class=&quot;oneRecordHomeFlexipage2Wrapper&quot;]/one-record-home-flexipage2[1]/forcegenerated-flexipage_lead_rec_l_lead__view_js[@class=&quot;forcegenerated-flexipage-module&quot;]/flexipage-record-page-decorator[1]/div[1]/slot[1]/flexipage-record-home-with-subheader-template-desktop2[@class=&quot;forcegenerated-flexipage-template&quot;]/div[@class=&quot;slds-grid slds-wrap&quot;]/div[@class=&quot;slds-col slds-size_1-of-1 row region-header&quot;]/slot[1]/slot[1]/flexipage-component2[1]/force-progressive-renderer[1]/slot[1]/slot[1]/records-lwc-highlights-panel[1]/records-lwc-record-layout[1]/forcegenerated-highlightspanel_lead___012000000000000aaa___compact___view___recordlayout2[@class=&quot;forcegenerated-record-layout2&quot;]/force-highlights2[1]/div[@class=&quot;highlights slds-clearfix slds-page-header slds-page-header_record-home fixed-position&quot;]/div[@class=&quot;secondaryFields&quot;]/slot[1]/slot[@class=&quot;slds-grid slds-page-header__detail-row&quot;]/force-highlights-details-item[@class=&quot;slds-page-header__detail-block&quot;]/div[1]/p[@class=&quot;fieldComponent slds-text-body--regular slds-show_inline-block slds-truncate&quot;]/slot[1]/lightning-formatted-text[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='brandBand_1']/div/div/div[2]/div/one-record-home-flexipage2/forcegenerated-flexipage_lead_rec_l_lead__view_js/flexipage-record-page-decorator/div/slot/flexipage-record-home-with-subheader-template-desktop2/div/div/slot/slot/flexipage-component2/force-progressive-renderer/slot/slot/records-lwc-highlights-panel/records-lwc-record-layout/forcegenerated-highlightspanel_lead___012000000000000aaa___compact___view___recordlayout2/force-highlights2/div/div[2]/slot/slot/force-highlights-details-item/div/p[2]/slot/lightning-formatted-text</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show 6 more actions'])[1]/following::lightning-formatted-text[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Note'])[1]/following::lightning-formatted-text[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Test'])[2]/preceding::lightning-formatted-text[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(123) 456-7899'])[1]/preceding::lightning-formatted-text[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//lightning-formatted-text</value>
   </webElementXpaths>
</WebElementEntity>
