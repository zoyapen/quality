<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AssociateOfISC2</name>
   <tag></tag>
   <elementGuidId>ae762dd0-5942-44bd-ad6b-db0acf14726f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-grid']/ul/li[2]/ul/li/div/div/div[2]/div/div[3]/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Certifications/Associate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Associate of (ISC)²                                                                                        Not enough experience? Start on a pathway to certification
                                                                                    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-grid&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;dropdown yamm-fw&quot;]/ul[@class=&quot;dropdown-menu on&quot;]/li[@class=&quot;grid&quot;]/div[@class=&quot;mm-item-v2&quot;]/div[@class=&quot;flx flx-main&quot;]/div[@class=&quot;flx-col cnt&quot;]/div[@class=&quot;flx flx-cat&quot;]/div[@class=&quot;flx-col cat col-sm-6&quot;]/ul[@class=&quot;nav&quot;]/li[3]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-grid']/ul/li[2]/ul/li/div/div/div[2]/div/div[3]/ul/li[3]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Architecture, Engineering, and Management Concentrations'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='World-Class Cybersecurity Training'])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Associate of (ISC)²']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Certifications/Associate')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/ul/li[3]/a</value>
   </webElementXpaths>
</WebElementEntity>
