<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>signInButton</name>
   <tag></tag>
   <elementGuidId>a3bb16e4-6a7c-4bd1-b73f-5d17809ee89c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='signIn__button--sign-in']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button button--block button--legacy-orange</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign In</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mfa-login&quot;)/div[@class=&quot;container mfa-wrapper&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6 login-left-column toggle-slider-box-1&quot;]/div[@class=&quot;col-md-12&quot;]/a[@class=&quot;button button--block button--legacy-orange&quot;]</value>
   </webElementProperties>
</WebElementEntity>
