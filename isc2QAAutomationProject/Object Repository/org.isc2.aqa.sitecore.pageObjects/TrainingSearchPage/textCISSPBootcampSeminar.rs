<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textCISSPBootcampSeminar</name>
   <tag></tag>
   <elementGuidId>b6cf884d-c726-4931-98c1-8470e001b820</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='row clearfix']//div[1]//div[1]//a[1]//h4[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>result</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                    
                        
                            CISSP BOOTCAMP Seminar
                        
                        Being the first information security certification accredited with the ANSI ISO recognition, the CISSP certification provides information security professionals with an objective measure of validity and professionalism recognized at an international level. The certification demonstrates advanced knowledge within the 8 domains of the (ISC) ² CISSP CBK.
                        Brought to you by: Internet Security Auditors
                    
                    
                    
                        
                            Start Date
                            03/16/2020
                        
                        
                            End Date
                            03/20/2020
                        
                        
                            Training Method
                            Classroom
                        
                        
                            Location
                            BARCELONA, Spain
                        

                        
                            
                            Learn More
                        
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-js&quot;]/body[@class=&quot;&quot;]/main[1]/section[@class=&quot;container-fluid section-container training-search-results&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row clearfix&quot;]/div[@class=&quot;col-xs-12 col-sm-12 col-md-12 col-lg-12 results&quot;]/div[@class=&quot;result&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sorting Options'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Training Solution Search Results'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
