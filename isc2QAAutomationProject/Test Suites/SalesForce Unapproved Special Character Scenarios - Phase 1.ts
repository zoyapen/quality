<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SalesForce Unapproved Special Character Scenarios - Phase 1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1d592d62-dd89-461f-9d15-c916d2ac675d</testSuiteGuid>
   <testCaseLink>
      <guid>64c6dc39-d36d-4890-a376-8270d24df729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Performance - Create Account via Sitecore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbab4ec6-a05c-4b3e-b33a-3435be3ec798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Performance - Create Account via Salesforce</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3bccde9-8d4a-41d1-8796-bf53cca94dda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/NightlyRegressionTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf46c103-8a12-481f-aa46-761f1094c0a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario 14</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
