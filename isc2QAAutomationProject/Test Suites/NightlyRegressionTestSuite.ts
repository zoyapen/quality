<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>NightlyRegressionTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cbb48704-d943-4277-9736-00b67e4ad44f</testSuiteGuid>
   <testCaseLink>
      <guid>b7686de6-1f44-4564-9841-d663700b3532</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/CreatingAccountViaSitecoreIsSuccessful</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8f1eceb-ac6e-4b1f-b7a0-6d823160a24f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/verifyCreatingAnAccountWithComplexEmailAndPasswordIsSuccessful</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>897da8ea-ed52-47e9-bd90-95cd142d03fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/LeadScenario</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b965ce84-b5c3-4b0e-ab5e-8282d29f52bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/TrainingFinder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>174b9cf4-fc28-4d37-97b4-f5360e7015b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/verifyCreatingANewAccountUsingUnapprovedSpecialCharactersDisplaysErrorMessage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54eb4707-539a-49a0-9934-318f259f0a74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/verifyCreatingNewUserAccountWithAccountInfoThatExistInSFFails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15db7a30-7fe2-47e1-b4c4-de9f5805d8fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/verifyFillingLeadFormwithMissingRequiredFieldsDisplaysError</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1e78420-27ae-4415-8ab0-16fd5acb6759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/verifyMemberVerificationWorksAsExpected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1511616-ae78-48ea-9b31-efd4b4c7ee96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/createNewUserAccountThatDoesNotMeetPasswordRequirements</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63e0628d-1028-44e5-9935-49cadc69eb7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/WWW-3872-verifyNavigationToMiniNavsWorksAsExpected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b581615-24df-463a-b881-a316db190c4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/Create an account via sitecore and verify contact in salesforce as a member service user - Sheila Jones</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
