import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.openBrowser('')

WebUI.navigateToUrl('https://test.salesforce.com/')

WebUI.setText(findTestObject('Object Repository/New/Salesforce/SF/Page_Login  Salesforce/input_Username_username'), 'sjones@isc2.org.qa')

WebUI.setEncryptedText(findTestObject('Object Repository/New/Salesforce/SF/Page_Login  Salesforce/input_Password_pw'), 'PIjsM1ujh+CO1P+8drIXRQFlNVYoDXUs')

WebUI.click(findTestObject('Object Repository/New/Salesforce/SF/Page_Login  Salesforce/input_Password_Login'))

WebUI.setText(findTestObject('Object Repository/New/Salesforce/SF/Page_Recently Viewed  Contacts  Salesforce/input_Search by object type_1730p'), 
    'aiyer@mailinator.com')

WebUI.sendKeys(findTestObject('Object Repository/New/Salesforce/SF/Page_Recently Viewed  Contacts  Salesforce/input_Search by object type_1730p'), 
    Keys.chord(Keys.ENTER))

WebUI.closeBrowser()

