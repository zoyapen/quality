import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.server.DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory

Date today = new Date()
String todaysDate = today.format('MM-dd-yyyy')
String currentTime = today.format('hh:mm:ss')
String currentDirectory = System.getProperty("user.dir");

int SHORT_TIMEOUT = 10;
int TIMEOUT = 20;
int LONG_TIMEOUT = 30;

WebUI.openBrowser('')

WebDriver driver = DriverFactory.getWebDriver()
WebDriverWait waitForTextToDisplay= new WebDriverWait(driver, SHORT_TIMEOUT)
WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, LONG_TIMEOUT)
WebUI.navigateToUrl(GlobalVariable.URL)

//WebUI.click(findTestObject('null'))

WebUI.navigateToUrl('https://wwwqa.isc2.org/MemberVerification')

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='field-last-name']")))

WebUI.setText(findTestObject('org.isc2.aqa.sitecore.pageObjects/MemberVerification/lastNameField'), 'Taylor')

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='field-member-number']")))

WebUI.setText(findTestObject('org.isc2.aqa.sitecore.pageObjects/MemberVerification/memberNumberField'), '408698')

waitForVisibilityOfElement.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='button button--block button--legacy-blue-border']")))

WebUI.click(findTestObject('org.isc2.aqa.sitecore.pageObjects/MemberVerification/searchButton'))

waitForTextToDisplay.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(),'408698')]")))
waitForTextToDisplay.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(),'Certified Information Systems Security Professiona')]")))
waitForTextToDisplay.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(),'Certified Secure Software Lifecycle Professional')]")))
waitForTextToDisplay.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Zyaire Taylor')]")))

WebUI.takeScreenshot((((currentDirectory + 'KatalonTestScreenshots/TestCase-MemberVerification/Scenario-MemberVerification/MemberVerificationDetails-' +
		todaysDate) + '_') + currentTime) + '.PNG')

WebUI.verifyTextPresent('Certified Information Systems Security Professional', false)

WebUI.verifyTextPresent('Certified Secure Software Lifecycle Professional', false)

WebUI.verifyTextPresent('Bjoern Voitel', false)

//WebUI.closeBrowser()

