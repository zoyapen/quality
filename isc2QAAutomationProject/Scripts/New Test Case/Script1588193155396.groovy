import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.openBrowser('')

WebUI.navigateToUrl('https://test.salesforce.com/')

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Login  Salesforce/input_Username_username'), 
    'aiyer@isc2.org.qa')

WebUI.setEncryptedText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Login  Salesforce/input_Password_pw'), 
    '870tJgtUtzursWhRp7b8IQ==')

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Login  Salesforce/input_Password_Login'))

WebUI.navigateToUrl('https://isc2--qa.lightning.force.com/one/one.app')

WebUI.navigateToUrl('https://isc2--qa.lightning.force.com/lightning/o/Lead/list?filterName=Recent')

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'), 
    'LF05-06-2020-03-38-37@mailinator.com')

WebUI.sendKeys(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37mailinatorcom - S_a23663/a_LF05-06-2020-03-38-37 Lead Form'))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_AutomatedLeadForm'))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_Automation Test'))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/a_(123) 456-7899'))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/span_Form Data'))

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/span_Campaign History'))

WebUI.closeBrowser()

//WebUI.verifyElementText(findTestObject(Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37 Lead Form  Salesforce/lightning-formatted-text_AutomatedLeadForm), 'AutomatedLeadForm')

