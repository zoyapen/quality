package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object SHORT_TIMEOUT
     
    /**
     * <p></p>
     */
    public static Object TIMEOUT
     
    /**
     * <p></p>
     */
    public static Object LONG_TIMEOUT
     
    /**
     * <p></p>
     */
    public static Object salesforce_id
     
    /**
     * <p></p>
     */
    public static Object salesforce_password
     
    /**
     * <p></p>
     */
    public static Object salesforceEmail
     
    /**
     * <p></p>
     */
    public static Object URL
     
    /**
     * <p></p>
     */
    public static Object LeadFormURL
     
    /**
     * <p></p>
     */
    public static Object memberVerificationURL
     
    /**
     * <p></p>
     */
    public static Object TrainingURL
     
    /**
     * <p></p>
     */
    public static Object actualName
     
    /**
     * <p></p>
     */
    public static Object lastName
     
    /**
     * <p></p>
     */
    public static Object email
     
    /**
     * <p></p>
     */
    public static Object leadFormemail
     
    /**
     * <p></p>
     */
    public static Object createAccountEmail
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            SHORT_TIMEOUT = selectedVariables['SHORT_TIMEOUT']
            TIMEOUT = selectedVariables['TIMEOUT']
            LONG_TIMEOUT = selectedVariables['LONG_TIMEOUT']
            salesforce_id = selectedVariables['salesforce_id']
            salesforce_password = selectedVariables['salesforce_password']
            salesforceEmail = selectedVariables['salesforceEmail']
            URL = selectedVariables['URL']
            LeadFormURL = selectedVariables['LeadFormURL']
            memberVerificationURL = selectedVariables['memberVerificationURL']
            TrainingURL = selectedVariables['TrainingURL']
            actualName = selectedVariables['actualName']
            lastName = selectedVariables['lastName']
            email = selectedVariables['email']
            leadFormemail = selectedVariables['leadFormemail']
            createAccountEmail = selectedVariables['createAccountEmail']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
