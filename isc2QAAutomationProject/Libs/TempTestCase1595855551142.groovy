import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner
import com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner
import com.kms.katalon.core.testng.keyword.internal.TestNGDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.testng.keyword.internal.TestNGDriverCleaner())


RunConfiguration.setExecutionSettingFile('/var/folders/r1/kt61sr9n49z4v55nss96qj2x06b8r7/T/Katalon/Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/Create an account via sitecore and verify contact in salesforce as a member service user - Sheila Jones/20200727_091230/execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/Create an account via sitecore and verify contact in salesforce as a member service user - Sheila Jones', new TestCaseBinding('Test Cases/org.isc2.sitecore.web.testCases/NightlyRegressionTest/Create an account via sitecore and verify contact in salesforce as a member service user - Sheila Jones',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
