package org.isc2.aqa.salesforce.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.Keys as Keys

class LeadsStepDefinition extends BaseSteps {

	@And("I navigate to Leads Page")
	def navigateToLeads() {
		WebUI.navigateToUrl('https://isc2--qa.lightning.force.com/lightning/o/Lead/list?filterName=Recent')
	}

	@And("I search for the lead that was just submitted")
	def searchForLeadThatWasJustSubmitted() {
		WebUI.delay(20)
		WebUI.refresh()
		WebUI.delay(20)
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'),
				GlobalVariable.leadFormemail)
		//WebUI.refresh()
		//WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'),
		//	'lf05-13-2020-11-22-15@mailinator.com')

		//WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'),'lf05-13-2020-11-27-13@mailinator.com')


		WebUI.sendKeys(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_Recently Viewed  Leads  Salesforce/input_Search by object type_1430p'),
				Keys.chord(Keys.ENTER))
	}

	@And("I open the searched lead")
	def openLead() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/Page_LF05-06-2020-03-38-37mailinatorcom - S_a23663/a_LF05-06-2020-03-38-37 Lead Form'))
		WebUI.delay(20)
	}

	@And("I navigate to details tab of the above lead")
	def navigateDetailsTabForLead() {
		WebUI.click(findTestObject('Object Repository/LEAD ASSERTIONS/Page_LF05-13-2020-12-17-26 Lead Form  Salesforce/a_Details'))
		WebUI.delay(20)
	}
}