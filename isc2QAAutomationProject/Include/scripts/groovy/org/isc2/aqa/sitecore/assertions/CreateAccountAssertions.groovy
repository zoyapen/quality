package org.isc2.aqa.sitecore.assertions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps
import org.openqa.selenium.support.ui.WebDriverWait

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.junit.After as After

class createAccountAssertions extends BaseSteps {

	int CA_LONG_TIMEOUT = 90
	WebDriver driver = DriverFactory.getWebDriver()
	WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, CA_LONG_TIMEOUT)

	@Then("I see a new account is been created for the above user")
	def iSeeNewAccountIsCreated() {
		//WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/VerifyTheAccountHolderName'))
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='icon signed-in signin-toggle']")))
		WebUI.delay(20)
		WebUI.verifyElementText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/VerifyTheAccountHolderName'), "HI, " + GlobalVariable.actualName)
		String ExpectedName = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/CreateAccountPage/VerifyTheAccountHolderName'))
		println("WOHOOOOOOOOOOO" + GlobalVariable.actualName)
	}
	
	@Then("I see the expected email address in personal information")
	def iSeeExpectedEmailAddressInPersonalInfo() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='card dashboard']//div[@class='card-body custom-left-padding']")))
		WebUI.verifyTextPresent(GlobalVariable.createAccountEmail, false)
	}


	@Then("I see error message for missing first name")
	def errorMessageForMissingFirstName() {
		WebUI.verifyTextPresent("First name is required", false)
	}

	@Then("I see error message for missing last name")
	def errorMessageForMissingLastName() {
		WebUI.verifyTextPresent("Last name is required", false)
	}

	@Then("I see error message for missing email")
	def errorMessageForMissingEmail() {
		WebUI.verifyTextPresent("Email is required", false)
	}

	@Then("I see error message for missing password")
	def errorMessageForMissingPassword() {
		WebUI.verifyTextPresent("Password is required", false)
	}

	@Then("I see error message for Privacy policy")
	def errorMessageForPrivacyPolicy() {
		WebUI.verifyTextPresent("You must accept the Privacy Policy to register", false)
	}

	@Then("I do not see error message for missing first name")
	def doNotSeeErrorMessageForMissingFirstName() {
		WebUI.verifyTextNotPresent("First name is required", false)
	}

	@Then("I do not see error message for missing last name")
	def doNotSeeerrorMessageForMissingLastName() {
		WebUI.verifyTextNotPresent("Last name is required", false)
	}

	@Then("I do not see error message for missing email")
	def doNotSeeerrorMessageForMissingEmail() {
		WebUI.verifyTextNotPresent("Email is required", false)
	}

	@Then("I do not see error message for missing password")
	def doNotSeeerrorMessageForMissingPassword() {
		WebUI.verifyTextNotPresent("Password is required", false)
	}

	@Then("I do not see error message for Privacy policy")
	def doNotSeeErrorMessageForPrivacyPolicy() {
		WebUI.verifyTextNotPresent("You must accept the Privacy Policy to register", false)
	}

	@Then("I see error message stating special characters are not allowed in first name field")
	def errorMessageSpecialCharactedFirstName() {
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-SiteCoreUnapprovedSpecialCharacters/Scenario-SiteCoreUnapprovedSpecialCharacters/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
		WebUI.verifyTextPresent("not allowed in the First Name field.", false)
	}

	@Then("I see error message stating special characters are not allowed in last name field")
	def errorMessageSpecialCharactedLasrName() {
		WebUI.verifyTextPresent("not allowed in the Last Name field.", false)
	}

	@Then("I see error related to password requirements")
	def errorRelatedToPasswordRequirements() {
		WebUI.verifyTextPresent("Your password must be at least 8 characters long, contain one or more numbers (0-9), one uppercase and one lowercase letter, and contain one or more of the following special characters", false)
		WebUI.takeScreenshot(((((currentDirectory + '-' + todaysDate + '-' + 'Screenshots/TestCase-PasswordRequirementsError/Scenario-PasswordRequirementsError/ErrorMessage-') +
				todaysDate) + '_') + currentTime) + '.PNG')
	}

	@Then("I see account creation fails")
	def iSeeAccountCreationFails() {
		WebUI.verifyTextPresent("The Email already in use, please try again", false)
	}
}