package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps


class MemberVerificationSteps extends BaseSteps {

	WebDriver driver = DriverFactory.getWebDriver()
	WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, LONG_TIMEOUT)

	@And("I navigate to member verification page")
	def navigateToMemberVerificationPage() {
		WebUI.navigateToUrl(GlobalVariable.memberVerificationURL)
	}

	@And("I enter last name field in the member verification form")
	def enterLastNameFieldMemberVerificationForm() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='field-last-name']")))
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/MemberVerification/lastNameField'), 'Taylor')
	}

	@And("I enter member number in the member verification form")
	def enterMemberNumberMemberVerificationForm() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='field-member-number']")))
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/MemberVerification/memberNumberField'), '408698')
	}

	@And("I search for the member in member verification form")
	def searchForMemberInMemberVerificationForm() {
		waitForVisibilityOfElement.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='button button--block button--legacy-blue-border']")))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/MemberVerification/searchButton'))
	}
}