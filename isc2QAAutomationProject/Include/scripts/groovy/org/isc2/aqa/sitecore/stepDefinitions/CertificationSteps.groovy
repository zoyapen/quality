package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps


class CertificationSteps extends BaseSteps {

	@And("I navigate to home page")
	def navigateToMemberVerificationPage() {
		WebUI.navigateToUrl(GlobalVariable.URL)
	}

	@And("I accept the cookies")
	def accpetCookies() {
		WebUI.waitForPageLoad(30)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/cookiesToastContainer'))
	}

	@And("I click the certifications drop-down")
	def clickCertificationsDropDown() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/Certifications'), 30)
		WebUI.mouseOver(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/Certifications'))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/Certifications'))
	}

	@And("I click Register for an Exam")
	def clickRegisterForExam() {
		WebUI.waitForElementVisible(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/RegisterForExam'),10)
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/RegisterForExam'))
	}

	@And("I click Endorsement")
	def clickEndorsement() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/Endorsement'))
	}

	@And("I click Member Verification")
	def clickMemberVerification() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/MemberVerification'))
	}

	@And("I click Digital Badges")
	def clickDigitalBadges() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/DigitalBadges'))
	}

	@And("I click CISSP")
	def clickCISSP() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/CISSP'))
	}

	@And("I click SSCP")
	def clickSSCP() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/SSCP'))
	}

	@And("I click CCSP")
	def clickCCSP() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/CCSP'))
	}

	@And("I click CAP")
	def clickCAP() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/CAP'))
	}

	@And("I click CISSP Concentrations")
	def clickCISSPConcentrations() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/CISSPConcentrations'))
	}

	@And("I click HCISSP")
	def clickHCISSP() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/HCISSP'))
	}

	@And("I click Associate of ISC2")
	def clickAssociateOfISC2() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/SiteCore/HomePage/AssociateOfISC2'))
	}
}
