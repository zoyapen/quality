package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps


class TrainingSearchResultSteps extends BaseSteps {

	WebDriver driver = DriverFactory.getWebDriver()
	WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, LONG_TIMEOUT)

	@And("I navigate to training finder search")
	def navigateToTrainingFinderSearch() {
		WebUI.navigateToUrl(GlobalVariable.TrainingURL)
	}

	@And("I click Advanced filters")
	def clickAdvancedFilters() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='button button--legacy-filter-dropdown']")))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/buttonAdvancedFilters'))
	}

	@And("I select certification type from the advanced filters drop-down")
	def selectCertificationTypeFromAdvancedFiltersDropDown() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id='certification']")))
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/selectCertificationType'), 'CISSP', true)
	}

	@And("I select country from the advanced filters drop-down")
	def selectCountryFromAdvancedFiltersDropDown() {
		waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[8]/li/div/div/select")))
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/selectCountry'), 'ES', true)
	}

	@And("I search for the above training")
	def searchForAboveTraining() {
		//waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row clearfix']//div[1]//div[2]//div[4]//span[2]")))
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/TrainingSearchPage/buttonSearch'))
	}
}