package org.isc2.aqa.sitecore.stepDefinitions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


import org.isc2.aqa.salesforce.stepDefinitions.BaseSteps

class LeadFormSteps extends BaseSteps {

	//String actualEmail = WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.salesforce.pageObjects/CreateContact/Page_New Contact  Salesforce/input_Email_14622063a'), email)
	String actualLeadFormFirstName = new String((((('LF' + todaysDate)))+ '-') + currentTime)
	String leadFormemail = new String(((('lf' + todaysDate) + '-') + currentTime) + '@mailinator.com')

	@And("I navigate to private-on-site lead form")
	def navigateToPrivateOnSiteLeadForm() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.LeadFormURL)
	}

	@And("I enter the first name in private-on-site lead form")
	def enterFirstNameInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/firstNameField'), actualLeadFormFirstName)
		println ("first name:- " + actualLeadFormFirstName)
	}

	@And("I enter the last name in private-on-site lead form")
	def enterLastNameInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/lastNameField'), 'Lead Form')
	}

	@And("I enter the job title in private-on-site lead form")
	def enterJobTitleInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/jobTitleField'), 'AutomatedLeadForm')
	}

	@And("I enter the company in private-on-site lead form")
	def enterCompanyInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/companyField'), 'Automation Test')
	}

	@And("I select the type of industry in private-on-site lead form")
	def enterTypeOfIndustryInPrivateOnSiteLeadForm() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownTypeOfIndustry'), 'Agriculture', true)
	}

	@And("I enter the email in private-on-site lead form")
	def enterEmailInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/scrollEmailField'), leadFormemail)
		println('Here is the email Id used:-' + leadFormemail)
		GlobalVariable.leadFormemail = leadFormemail
		//println(GlobalVariable.leadFormemail)
		//WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/scrollEmailField'), 'CMRScenario5RegressionTest1209@mailinator.com')

	}

	@And("I enter the phone number in private-on-site lead form")
	def enterPhoneNumberInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/phoneNumberField'), '1234567899')
	}

	@And("I select State or Province in private-on-site lead form")
	def enterStateProvinceInPrivateOnSiteLeadForm() {
		WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/addressStateField'), 'MH')
	}

	@And("I select Country in private-on-site lead form")
	def selectCountryInPrivateOnSiteLeadForm() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/addressCountryField'), 'Australia', true)
	}

	@And("I select Ceritification of Interest in private-on-site lead form")
	def selectCertificationOfInterestInPrivateOnSiteLeadForm() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownCertificationOfInterest'), 'CSSLP', true)
	}

	@And("I select Number of Employees to be trained in private-on-site lead form")
	def selectNoOfEmployeesInPrivateOnSiteLeadForm() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownNoOfEmployees'), '10-19', true)
	}

	@And("I select Time for Training in private-on-site lead form")
	def selectTimeofTrainingInPrivateOnSiteLeadForm() {
		WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownTrainingTimeline'), '4-6 Months', true)
	}

	@And("I have read and agree to the privacy policy in private-on-site lead form")
	def readAgreeToThePrivacyPolicyInPrivateOnSiteLeadForm() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/agreePolicyCheckbox'))
	}

	@And("I submit private-on-site lead form")
	def submitPrivateOnSiteLeadForm() {
		WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/submitButton'))
	}

	@And("I end the session")
	def endSession() {
		WebUI.navigateToUrl('https://wwwqa.isc2.org/api/coretest/endsession')
	}
}