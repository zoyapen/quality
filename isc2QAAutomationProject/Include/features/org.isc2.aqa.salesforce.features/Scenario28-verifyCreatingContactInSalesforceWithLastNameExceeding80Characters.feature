Feature: Scenario-29 Creating a contact with last name exceeding 40 characters is not successful

  Scenario Outline: Verify that creating a contact with last name exceeding 40 characters is not successful
    Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter last name as <lastNameValue>
    And I enter email address as <emailAddressValue>
    And I click save
    Then I see an error message
    	Examples:
    		|firstNameValue		|lastNameValue																																		 	 |emailAddressValue											 |
    		|Test							|ThisIsMyLast1NameGuessWhatCMRGoLive1SystThisIsMyLast1NameGuessWhatCMRGoLive1Syst1   |testingTimeForCMRGoLive1@mailinator.com |
    		