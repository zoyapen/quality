Feature: Scenario-24 Creating a new contact in salesforce is successful

  Scenario Outline: Create New Contact from Salesforce using Email that exist in Salesforce
  	Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter last name as <lastNameValue>
    And I enter email address for sf contact <email>
    And I click save
    	Examples:
    		|firstNameValue				 |lastNameValue							 |email|
    		|PerformanceTest			 |QualityAssurance					 |iqqa.tester@tech.com.com.i.brs.invalid|
    		|PerformanceTest			 |QualityAssurance					 |iqqqa.tester@te.e.ch.com.com.its.invalid|
    		|PerformanceTest			 |QualityAssurance					 |itttester.qa@tqe.q.echom.com.itss.invalid|
    		|PerformanceTest			 |QualityAssurance					 |iqqqa.tester@mai.l.i.nator.com.invalid|