 Feature: Creating an account via sitecore is susccessful 
    
     Scenario: Verify that creating an account via sitecore is susccessful 
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter an email for creating an account
    And I enter last name for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
 		And I close a browser