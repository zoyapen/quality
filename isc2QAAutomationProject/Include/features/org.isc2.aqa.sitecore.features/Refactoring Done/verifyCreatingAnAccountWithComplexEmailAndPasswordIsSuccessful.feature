 Feature: Creating an account via sitecore is successful with complex email and password
    
  Scenario Outline: Verify that creating an account via sitecore is succcessful with complex email
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter last name for creating an account
    And I enter an email for creating an account using <complicatedEmail>
    And I confirm email for creating an account using <complicatedEmail>
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see a new account is been created for the above user
    	Examples:
    		|complicatedEmail							|
    		|IQQQa.tester@ttech.com.it		|
				|ITTTester.QA@ttech.h.com.it  |
		
		
	Scenario Outline: Verify that creating an account via sitecore is successful with complex password
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter last name for creating an account
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter complex <password> for creating an account
    And I confirm complex <password> for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see a new account is been created for the above user
    	Examples:
    		|password|
				|Arsenal8^^|
				|Ar^senal8^|
				|Ars^nal8^^|
				
				