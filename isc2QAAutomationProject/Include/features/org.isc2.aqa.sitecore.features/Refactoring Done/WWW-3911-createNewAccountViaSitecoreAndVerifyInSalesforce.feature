 Feature: Create an account via sitecore and verify contact in salesforce as a member service user - Sheila Jones 
    
  Scenario: Verify that creating an account via sitecore and searching the same contact in salesforce as a member service user - Sheila Jones returns expected result
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter an email for creating an account
    And I enter last name for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    And I click the dropdown for navigation
    And I navigate to my profile
    Then I see the expected email address in personal information
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to automated contacts page
    When I search for the contact in sitecore automation contacts page
    Then I see the correct email for the above contact
    And I see the correct contact owner as mciamser
    And I see CREATED for Okta status
    And I see CREATED for WebDB status
    And I see CREATED for Salesforce status
    And I see CREATED for D2L status
    And I close a browser 
    
    

    