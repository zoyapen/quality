Feature: Verify that that navigation to Mini-navs works as expected for a member and non-member user

  Scenario: Verify that that navigation to Mini-navs works as expected for a member user
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I enter email address for a member user
    And I enter password for a member user
    And I click sign in button
    Then I see member login is successful
    When I navigate to Member Home
    Then navigation to Member Home is successful
    When I navigate to Dashboard
    Then navigation to Dashboard is successful
    When I navigate to Profile
    Then navigation to Profile is successful
    When I navigate to Preferences
    Then navigation to Preferences is successful
    When I navigate to Benefits
    Then navigation to Benefits is successful
    When I navigate to Maintenance Fee
    Then navigation to Manintenance Fee is successful
    When I navigate to CPEs
    Then navigation to CPEs is successful
    And I close a browser

  Scenario: Verify that that navigation to Mini-navs works as expected for a non-member user
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I enter email address for a non-member user
    And I enter password for a non-member user
    And I click sign in button
    When I navigate to Profile for a non-member
    Then navigation to Profile for a non-member is successful
    When I navigate to Preferences for a non-member
    Then navigation to Preferences is successful
    And I do not see Benefits mini-nav
    And I do not see Maintenance Fee mini-nav
    And I close a browser
    
    