  Feature: Create new account that does not meet password requirements displays error
  
   Scenario: Verify that filling a lead form is susccessful 
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter an email for creating an account
    And I enter last name for creating an account
    And I confirm email for creating an account
    And I enter password that does not meet password requirements
    And I click create a new account button
    Then I see error related to password requirements