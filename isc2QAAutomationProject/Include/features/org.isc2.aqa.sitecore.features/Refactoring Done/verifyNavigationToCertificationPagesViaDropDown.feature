Feature: Navigation to certification pages via certification drop-down list

  Scenario: Verify navigation to certification pages via certification drop-down list
    Given I open a browser
    And I navigate to home page
    And I accept the cookies
    And I click the certifications drop-down
    When I click Register for an Exam
    Then I see the correct title on registration page
    And I click the certifications drop-down
    When I click Endorsement
    Then I see the correct title on endorsements page
    And I click the certifications drop-down
    When I click Member Verification
    Then I see the correct title on member verification page
    And I click the certifications drop-down
    When I click Digital Badges
    Then I see the correct title on digital badges page
    And I click the certifications drop-down
    When I click CISSP
    Then I see the correct title on CISSP page
    And I click the certifications drop-down
    When I click HCISSP
    Then I see the correct title on HCISSP page
    And I click the certifications drop-down
    When I click Associate of ISC2
    Then I see the correct title on Associate of ISC2 page
    And I click the certifications drop-down
    When I click CISSP Concentrations
    Then I see the correct title on CISSP Concentrations page
    And I click the certifications drop-down
    When I click CAP
    Then I see the correct title on CAP page
    And I click the certifications drop-down
    When I click SSCP
    Then I see the correct title on SSCP page
    