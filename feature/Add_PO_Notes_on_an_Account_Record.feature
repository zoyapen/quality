Feature: Add PO Notes on an Account Record

  Scenario:
    Given Finance User is logged in Account record
    And User is on the Addiotional information
    When User clicks on the Add PO Notes
    Then User can enter PO Notes and save
