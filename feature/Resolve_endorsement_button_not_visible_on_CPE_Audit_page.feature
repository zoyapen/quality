Feature: Resolve endorsement button not visible on CPE Audit page
Customer support user should not see Resolve Endorsement button on CPE Audit page

  Scenario: Cusotmer support user resolving CPE audit
   Given CPE is submitted through portal
   And is selected for audit
   When CPE specialist navigates to CPE Audit page to resolve the CPE
   Then no 'Resolve Endorsement' button present on page layout
