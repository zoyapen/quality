Feature: During the AMF transition period Reminder Emails should be sent for AMF due or an unpaid transition invoice
  

  Scenario:
    Given member has AMF due or an unpaid transition invoice
    When they have the renewal and transition
    Then they have the renewal and transition or the $ amt due
