Feature: Contact added to right chapter when multiple countries has full or partial matching zipcode
 

  Scenario: Two countries has partial matching zipcode
    Given member has address with Country listed as Netherlands
    And zip code that is matching partially to Australia country
    And Netherlands Chapter has no zip codes associated to it in X-ref file 
    When batch job is executed 
    Then member is assigned to Netherlands chapter territory

 Scenario:Two countries has full matching zipcode
    Given member has address with Country listed as Australia 
    And zip code that is matching fully to other country in Xref file
    When batch job is executed
    Then member is assigned to Australia chapter territory
    
 Scenario: Two countries has partial matching zipcode
    Given member is not a Community Group Member 
    And relocates to a place that has active chapter territory
    When member updates the address with Country listed as Australia
    And zip code that is matching partially to United States in Xref file
    Then member is assigned to Australia chapter territory