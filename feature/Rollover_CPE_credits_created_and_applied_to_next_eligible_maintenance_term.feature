Feature: Rollover CPE credits created and applied to next eligible maintenance term
  Any Group A CPEs earned over the Total CPE requirement in last 6 months of the cycle can roll over and the maximum limit is upto 40 credits. 

  Scenario: Member has satisfied minimum CPE requirement and submits Group A CPE within last 6 months of the end of the term
    

  Scenario: Member has satisfied minimum CPE requirement and submits Group B CPE within last 6 months of the end of the term
    
    
  Scenario: Member has satisfied minimum CPE requirement and submits Group A CPE 7 months before the end of the term
  
  
  Scenario: Member has satisfied minimum CPE requirement and submits Group B CPE 7 months before the end of the term
    
    
  Scenario Outline: Member has satisfied minimum CPE requirement and submits Group A CPE at the end of the term and goes beyond max Rollover credits
    
    
  Scenario: Member has outstanding total CPE requirement and submits Group A CPE within last 6 months of the end of the term
    
 
  Scenario: Member has outstanding total CPE requirement and submits Group B CPE within last 6 months of the end of the term


  Scenario: Member has outstanding total CPE requirement and submits large chunk of Group A CPE at the end of the term which satisfies total CPEs goal


  Scenario: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date within term dates
   
  
  Scenario: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date within term dates


  Scenario: Member in Grace period with outstanding total CPE requirement submits group A CPE with activity date outside of term dates


  Scenario: Member in Grace period with outstanding total CPE requirement submits group B CPE with activity date outside of term dates