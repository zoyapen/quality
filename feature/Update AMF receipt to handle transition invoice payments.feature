Feature: Update AMF receipt to handle transition invoice payments
  

  Scenario: Display AMF receipt with  correct unit price or quantity for transition fee is paid
    Given 2019 AMF transition invoice that has been paid
    When the CRM report "AMF Receipt - AMF Transition" is executed for the invoice
    Then the resulting AMF receipt displays the correct unit price and quantity of 1
