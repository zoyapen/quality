Feature: Renewal reminder sent at specific time before and after the maintenace term end date
  Renewal Reminder sent 45 days and 15 days before expiration date and 15 days, 45 days and 75 days after the maintenance term

  Scenario: Member has incomplete goals and active term is about to expire
    Given Member has an active maintenance term
    And CPE/AMF goal is pending
    When current active maintenance term is about to expire in next 45 days
    Then an expiration reminder email is sent to members for first time
    When current active maintenance term is about to expire in next 15 days
    Then an expiration reminder email is sent to members for second time
    When current active maintenance term has expired and 15 days has elapsed (in grace period)
    Then an expiration reminder email is sent to members for third time
    When current active maintenance term has expired and 45 days has elapsed (in grace period)
    Then an expiration reminder email is sent to members for fourth time
    When current active maintenance term has expired and 75 days has elapsed (in grace period)
    Then an expiration reminder email is sent to members for fifth time
